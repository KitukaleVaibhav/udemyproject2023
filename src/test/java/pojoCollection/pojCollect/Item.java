
package pojoCollection.pojCollect;

import java.util.List;
public class Item {

    private String name;
    private List<Event> event;
    private Request request;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Event> getEvent() {
        return event;
    }

    public void setEvent(List<Event> event) {
        this.event = event;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Item(String name, List<Event> event, Request request) {
        this.name = name;
        this.event = event;
        this.request = request;
    }
}
