
package pojoCollection.pojCollect;

import java.util.List;
public class Collection {

    private Info info;
    private List<Item> item;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public Collection(Info info, List<Item> item) {
        this.info = info;
        this.item = item;
    }
}
