package pojoPractise2024.pojoClasses;

import java.util.HashMap;

public class MainObject {
private Personal personal;
private Professional professional;

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public Professional getProfessional() {
        return professional;
    }

    public void setProfessional(Professional professional) {
        this.professional = professional;
    }

    public MainObject(Personal personal, Professional professional) {
        this.personal = personal;
        this.professional = professional;
    }
}
