package pojoPractise2024.pojoClasses;

import java.util.List;

public class Root1 {
  private List<MainObject> l1;

    public List<MainObject> getL1() {
        return l1;
    }

    public void setL1(List<MainObject> l1) {
        this.l1 = l1;
    }

    public Root1(List<MainObject> l1) {
        this.l1 = l1;
    }
}
