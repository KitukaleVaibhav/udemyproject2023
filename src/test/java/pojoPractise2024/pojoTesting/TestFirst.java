package pojoPractise2024.pojoTesting;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.testng.annotations.Test;
import pojoPractise2024.pojoClasses.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestFirst {

    @Test
    public void checkPojo() throws JsonProcessingException {

        Personal personall = new Personal("Vaibhav", "patil");
        Professional professionall = new Professional(90222, "sdet");

        MainObject mainObject=new MainObject(personall,professionall);

        //PersonalObject po=new PersonalObject("personal",personall);
       /* HashMap<String, Object> h1 = new HashMap<>();
        h1.put("personal",personall);
        h1.put("professional",professionall);*/
        List<MainObject> l1=new ArrayList<>();
        l1.add(mainObject);
        Root1 r1=new Root1(l1);
        ObjectWriter objectMapper = new ObjectMapper().writerWithDefaultPrettyPrinter();// we use this to show response in good format
        String payload = objectMapper.writeValueAsString(l1);
        System.out.println(payload);

    }
}
