package gmailApi.testClasses;

import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.authentication.FormAuthConfig;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Base64;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.specification.ProxySpecification.auth;

public class TestGmailAPi {

    RequestSpecification requestSpecification;

    public String getaccesToken(){

        Response response=given().baseUri("https://oauth2.googleapis.com")
                .formParam("client_id", ConfigLoader.getInstance().getPropertyValue("clientId"))
                .formParam("client_secret",ConfigLoader.getInstance().getPropertyValue("clientSecret"))
                .formParam("grant_type","refresh_token")
                .formParam("refresh_token",ConfigLoader.getInstance().getPropertyValue("refreshToken")).log().all()
                .when().post("/token").then().log().all().assertThat().statusCode(200).extract().response();
        String ReceivedAccessToken=response.path("access_token");
        return ReceivedAccessToken;
    }
   // @BeforeClass
    public void beforeClass() {
        String ReceivedToken=getaccesToken();
        String finalToken="Bearer "+ReceivedToken;
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://gmail.googleapis.com")
                .addHeader("Authorization",  finalToken)
                .setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();// if we use the build the it'Root1 mandatory to use  given().spec(requestSpecification); requestSpecifoication format

    }
@Test
    public void getProfile() {
        given(requestSpecification).basePath("/gmail/v1")
                .pathParam("userid","vaibhavkitukale2023@gmail.com")
                .when()
                .get("/users/{userid}/profile")
                .then().assertThat().statusCode(200).log().all();

    }
    @Test
    public void sendMessage(){
        System.out.println("send msg method start");
        String email4="From: vaibhavkitukale2023@gmail.com\n" +
                "To: vaibhavkitukale2023@gmail.com\n" +
                "Subject: email on 16 july 2024 msg number 2\n" +
                "\n" +
                "testing gmail\n";
       String payloadBase64Encoded= Base64.getUrlEncoder().encodeToString(email4.getBytes());
        HashMap<String,String> payload=new HashMap<String, String>();
        payload.put("raw",payloadBase64Encoded);
        given(requestSpecification).basePath("gmail/v1/users")
                .pathParam("userId","vaibhavkitukale2023@gmail.com")
                .body(payload).log().body()
                .when().post("/{userId}/messages/send")
                .then().log().all();
    }

    @Test
    public void formBased(){
       String sessionId =given().baseUri("http://localhost:8181/ServletProject1")
                .formParam("username","vaibhavkitukale2017@gmail.com")
                .formParam("password","123456789")
                .when().post("/loginController")
                .then().log().all().extract().response().getSessionId();
       System.out.println("sessionId: "+sessionId);
    }

    @Test
    public void formBasedAuthPart2(){
        given().baseUri("http://localhost:8181/ServletProject1")
                .auth().form("vaibhavkitukale2017@gmail.com","123456789",new FormAuthConfig("/loginController","username","password"))
                .when().post("/loginController")
                .then().log().all();
    }
}
