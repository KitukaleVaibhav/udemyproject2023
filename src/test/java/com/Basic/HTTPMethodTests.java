package com.Basic;

import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesRegex;

public class HTTPMethodTests {
    RequestSpecification requestSpecification;

    @BeforeClass
    public void beforeClass() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"));
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .expectStatusLine("HTTP/1.1 200 ");
        responseSpecification = responseSpecBuilder.build();

    }

    @Test
    public void postRequest() {
        File filePayload=new File("src/test/resources/PostRequestPayload.json");
        String paylaod = "{\n" +
                "    \"workspace\": {\n" +
                "        \"name\": \"workspace 12 july\",\n" +
                "        \"type\": \"personal\",\n" +
                "        \"description\": \"created in12 july 2024 but automation run 3\"\n" +
                "    }\n" +
                "}";
        given().spec(requestSpecification).body(paylaod)
                .when()
                .post("/workspaces")
                .then().spec(responseSpecification)
                .log().all();
    }

    //   @Test
    public void putRequest() {
        String workspaceId = "846d7285-2943-47e0-8e0c-3100858e5df0";
        String payload = "{\n" +
                "    \"workspace\": {\n" +
                "        \"name\": \"workspace1 updated 05-11-2023\",\n" +
                "        \"type\": \"personal\",\n" +
                "        \"description\": \"workspace 1 updated for PUT method test\"\n" +
                "    }\n" +
                "}";

        given().body(payload)
                .when()
                .put("/workspaces/" + workspaceId)
                .then()
                .log()
                .all()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void deleteRequest() {
        String workspaceId = "1b4a1318-0a84-4ac8-a297-44e7233e469f";
        given().spec(requestSpecification)
                .when()
                .delete("/workspaces/" + workspaceId)
                .then()
                .log().all()
                .assertThat().statusCode(200);

    }
}
