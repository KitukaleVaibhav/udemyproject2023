package com.Basic;

import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

import static io.restassured.RestAssured.*;

public class SendPayloadByDifferentTypes {

    @BeforeClass
    public void beforeClass() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"));
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .expectStatusLine("HTTP/1.1 200 OK");
        responseSpecification = responseSpecBuilder.build();

    }

    @Test
    public void sendPayloadByFile(){
        String workspaceId = "846d7285-2943-47e0-8e0c-3100858e5df0";
        File file=new File("src/test/resources/PutRequestPayload.json");
        given(requestSpecification).body(file)
                .when()
                .put("/workspaces/"+workspaceId)
                .then()
                .log()
                .all()
                .assertThat()
                .statusCode(200);
    }
}
