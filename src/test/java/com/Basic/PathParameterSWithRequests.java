package com.Basic;

import SpotifyApiTesting.api.RouteUrl;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PathParameterSWithRequests {

    @Test
    public void pathParamUse(){
            given().baseUri("https://reqres.in").basePath(RouteUrl.BASE_PATH)
                    .pathParam("id",2)
                    .log().uri()
                    .when().get("/users/{id}")
                    .then().log().all();
        }

    }

