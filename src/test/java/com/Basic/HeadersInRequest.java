package com.Basic;

import io.restassured.config.LogConfig;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;

public class HeadersInRequest {

    @Test
    public void headersInRequest() {
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .header("header1", "value1")
                .header("x-mock-match-request-headers", "header1")
                .get("/getMockHeader").then()
                .assertThat().statusCode(200).log().all();
    }

    @Test
    public void sendMultipleHeadersUsingHeaders() {
        Header header = new Header("header2", "value2");
        Header matchHeader = new Header("x-mock-match-request-headers", "header2");
        Headers headers = new Headers(header, matchHeader);
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .headers(headers).log().all()
                .get("/getMockHeader").then()
                .assertThat().statusCode(200).log().all();

    }

    @Test
    public void sendMultipleHeadersUsingHashmap() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("header1", "value1");
        headers.put("x-mock-match-request-headers", "header1");
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .headers(headers).log().all()
                .get("/getMockHeader").then()
                .assertThat().statusCode(200);
    }

    @Test
    public void multiValueHeaderInRequest() {
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .header("mutlivalueHeader", "head1", "head2").log().all().when().
                get("/getMockHeader").then()
                .assertThat().statusCode(200);
    }

    @Test
    public void verifySingleResponseHeaders() {
      Response response=  given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .header("mutlivalueHeader", "head1", "head2").log().all().when().
                get("/getMockHeader").then().log().all()
                .assertThat().statusCode(200).header("response-header2", "header2").extract().response();
      System.out.println("hv: "+response.header("response-header2"));
        Assert.assertEquals(response.header("response-header2"),"header1");
    }

    @Test
    public void verifyMultipleResponseHeaders() {
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .header("mutlivalueHeader", "head1", "head2").log().all().when().
                get("/getMockHeader").then().log().all()
                .assertThat().statusCode(200).headers("X-RateLimit-Limit", "120",
                        "Content-Type", "application/json; charset=utf-8", "response-header2", "header2");
    }

    @Test
    public void extractResponseHeaders() {
        Headers extractedHeaders = given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .header("mutlivalueHeader", "head1", "head2").log().all().when().
                get("/getMockHeader").then().log().all()
                .extract().headers();
        System.out.println("extracted HeaderName: " + extractedHeaders.get("X-RateLimit-Limit").getName());
        System.out.println("extracted HeaderValue: " + extractedHeaders.get("X-RateLimit-Limit").getValue()); // it return header vaule

    }
}
