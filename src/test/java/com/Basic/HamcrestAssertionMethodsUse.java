package com.Basic;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class HamcrestAssertionMethodsUse {
    @Test
    public void hamcrestAsertionMethods() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when().get("/workspaces").then().log().all()
                .assertThat().statusCode(200)
                .body("workspaces.name", containsInAnyOrder("Team WorkspaceTrial", "My WorkspaceTrial", "Workspace1", "workspace2"),
                        "workspaces.name", contains("My WorkspaceTrial", "Team WorkspaceTrial", "workspace2"),
                        "workspaces.name", is(not(empty())),
                        "workspaces.size()", hasSize(24),
                        //"workspaces[0]", hasKey("name"),
                        "workspaces[0]", hasValue("workspace3"),
                        "workspaces[0]", hasEntry("name","personal"),
                        "workspaces[0].name", startsWith("w")

                        );


    }


}
