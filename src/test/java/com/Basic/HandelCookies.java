package com.Basic;

import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.Set;

import static io.restassured.RestAssured.given;

public class HandelCookies {
    @Test
    public void sendSingleCookies() {

        Header header = new Header("header2", "value2");
        Header matchHeader = new Header("x-mock-match-request-headers", "header2");
        Headers headers = new Headers(header, matchHeader);
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .headers(headers).cookie("JSESSIONID", "3903-aAba-3435485948").log().all()
                .get("/getMockHeader").then()
                .assertThat().statusCode(200).log().all();
    }

    @Test
    public void sendCookieUsingCookieBuilder() {
        Header header = new Header("header2", "value2");
        Header matchHeader = new Header("x-mock-match-request-headers", "header2");
        Headers headers = new Headers(header, matchHeader);

        Cookie cookie = new Cookie.Builder("JSESSIONID", "SDM84390-439-5NDFK").build();
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .headers(headers).cookie(cookie).log().all()
                .get("/getMockHeader").then()
                .assertThat().statusCode(200).log().all();
    }

    @Test
    public void sendMultipleCookies() {
        Header header = new Header("header2", "value2");
        Header matchHeader = new Header("x-mock-match-request-headers", "header2");
        Headers headers = new Headers(header, matchHeader);
        Cookie cookie1 = new Cookie.Builder("JSESSIONID1", "SDM84390-439-5NDFK-90").build();
        Cookie cookie2 = new Cookie.Builder("JSESSIONID2", "SDM84390-439-5NDFK-91").build();
        Cookie cookie3 = new Cookie.Builder("JSESSIONID3", "SDM84390-439-5NDFK-92").build();
        Cookies cookies = new Cookies(cookie1, cookie2, cookie3);
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/")
                .headers(headers).cookies(cookies).log().all()
                .get("/getMockHeader").then()
                .assertThat().statusCode(200).log().all();
    }

    @Test
    public void fetchSingleCookie() {
        Header header = new Header("header2", "value2");
        Header matchHeader = new Header("x-mock-match-request-headers", "header2");
        Headers headers = new Headers(header, matchHeader);
        Cookie cookie1 = new Cookie.Builder("JSESSIONID1", "SDM84390-439-5NDFK-90").build();
        Cookie cookie2 = new Cookie.Builder("JSESSIONID2", "SDM84390-439-5NDFK-91").build();
        Cookie cookie3 = new Cookie.Builder("JSESSIONID3", "SDM84390-439-5NDFK-92").build();
        Cookies cookies = new Cookies(cookie1, cookie2, cookie3);
        RequestSpecification request = given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io/").cookies(cookies).log().cookies();
        Response response = request.when().get("/getMockHeader").then().extract().response();
        response.then().log().all();
        Map<String, String> cookies1 = response.then().extract().cookies();
        Set<Map.Entry<String, String>> s1 = cookies1.entrySet();
        for (Map.Entry<String, String> s2 : s1) {
            System.out.println("coikeName:" + s2.getKey() + " cookieValue:" + s2.getValue());
        }
    }
}
