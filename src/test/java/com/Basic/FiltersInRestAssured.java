package com.Basic;

import SpotifyApiTesting.utils.ConfigLoader;
import com.fasterxml.jackson.databind.ser.BeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.PrintStream;

import static io.restassured.RestAssured.given;


public class FiltersInRestAssured {
    private static final ThreadLocal<String> currentTestName = new ThreadLocal<>();
    ResponseSpecification responseSpecification;
    RequestSpecification requestSpecification;

    @BeforeClass
    public void responseSpecBuilderClassUse() throws FileNotFoundException {
        PrintStream fileoutputStream = new PrintStream(new FileOutputStream("restassured1.log", true));
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder()
                .addFilter(new RequestLoggingFilter(fileoutputStream))
                .addFilter(new ResponseLoggingFilter(fileoutputStream));
        RestAssured.requestSpecification = requestSpecBuilder.build();
        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        RestAssured.responseSpecification = responseSpecBuilder.build();
    }

    @Test
    public void testfilters1() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .filter(new RequestLoggingFilter())
                .filter(new ResponseLoggingFilter()) // using LogDetail enum we can print/ log the things as per our requirement
                .when().get("/workspaces");

    }

    //   @Test
    public void logggingFilter() throws FileNotFoundException {
        PrintStream fileoutputStream = new PrintStream(new PrintStream("restassured.log"));
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .filter(new RequestLoggingFilter(fileoutputStream))
                .filter(new ResponseLoggingFilter(fileoutputStream)) // we use this to log our response for debugging purpose
                .when().get("/workspaces");
    }

    @Test
    public void logggingFilterTest2() throws FileNotFoundException {
        PrintStream fileoutputStream = new PrintStream(new PrintStream("restassured.log"));
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .filter(new RequestLoggingFilter(LogDetail.HEADERS, fileoutputStream))
                .filter(new ResponseLoggingFilter(LogDetail.STATUS, fileoutputStream)) // here we are logging only those part which we wan't
                .when().get("/workspaces");
    }


    // @Test
    public void reuseFilterForOtherTestCases() {
        // we mention filers code in before class method so it will reuse for every method
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .when().get("/workspaces");

    }

    @Test
    public void testFilter2() {
        given().baseUri("https://api.getpostman.com")
                .filter(new ModifiedFilter()).log().all()
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .when().get("/workspaces").then().log().all();
    }

}

class ModifiedFilter implements Filter {

    @Override
    public Response filter(FilterableRequestSpecification filterableRequestSpecification, FilterableResponseSpecification filterableResponseSpecification, FilterContext filterContext) {
        filterableRequestSpecification.header("username", "vaibhav");
        filterableResponseSpecification.header("Connection", "keep-alive");
        return filterContext.next(filterableRequestSpecification, filterableResponseSpecification);


    }
}

