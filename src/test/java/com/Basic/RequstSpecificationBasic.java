package com.Basic;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RequstSpecificationBasic {
    RequestSpecification request;

    @Test
    public void requestPscBasic() {
    /*
    The RequestSpecification class is a part of the Java-based REST API testing framework,
     called RestAssured. It is used to define the request that will be sent to a REST API endpoint
     and to specify the expected response.The RequestSpecification class provides a fluent API
     that allows you to specify various aspects of the request, such as headers, query parameters,
     request body, and response expectations. For example, you can use the header method to set a
      specific header for the request, or the body method to set the request body to a specific
      JSON or XML string.Once you have defined the RequestSpecification object, you can use it to
      send a request to a REST API endpoint using the given method of the RestAssured class.
      The given method takes the RequestSpecification object as a parameter and returns a
       Response object that contains the response from the API endpoint.Here'Root1 an example
       of how to use the
    RequestSpecification class to send a GET request to a REST API endpoint:
     */
        request = RestAssured.given();
        request.baseUri("https://api.getpostman.com");
        request.header("Content-Type", "application/json");
        request.header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3");
        Response response = request.get("/workspaces");
        System.out.println(response.then().log().all());

    }

    @Test
    public void validateStatusCode(){
        request=RestAssured.given();
        request.baseUri("https://api.getpostman.com");
        request.header("Content-Type", "application/json");
        request.header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3");
        request.log().all();
        Response response = request.get("/workspaces");
        assertThat(response.statusCode(),equalTo(200));
    }
}
