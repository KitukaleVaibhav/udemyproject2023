package com.Basic;

import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import java.io.File;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;

public class AutomateFormUrlEnocedeRequestPayload {

    @Test
    public void formUrlEncoded(){ // we mostly use this while working with outh 2.0 authorization
        given().baseUri("https://postman-echo.com")
                .config(config().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))// here we telling to rest assured not to set default content charset if content type is undefined
                .formParam("Key1","value1")
                .formParam("Key2","value2")
                .formParam("Key3","value3")
                .log().all()
                .when()
                .post("/post")
                .then().log().all().assertThat().statusCode(200);
    }

}

