package com.Basic;

import io.restassured.config.LogConfig;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;

public class RequestResponseLogging {
    @Test
    public void requestLogging() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a").log().all()
                .when()
                .get("/workspaces").then()
                .assertThat().statusCode(200);
    }

    @Test
    public void responseLogging() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when()
                .get("/workspaces").then().log().all()
                .assertThat().statusCode(200);
    }
    @Test
    public void logOnlyIfError() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3addStrintoGetError")
                .when()
                .get("/workspaces").then().log().ifError()
                .assertThat().statusCode(200);
    }
    @Test
    public void logOnlyIfvalidationFails() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when()
                .get("/workspaces").then().log().ifValidationFails()
                .assertThat().statusCode(201);
    }

    @Test
    public void blacklistHeader(){
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .config(config.logConfig(LogConfig.logConfig().blacklistHeader("x-api-key")))
                .when().log().all()
                .get("/workspaces").then()
                .assertThat().statusCode(200);
    }

    @Test
    public void responseSomePart() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when()
                .get("/workspaces").then().log().all()
                .assertThat().statusCode(200);
    }
}
