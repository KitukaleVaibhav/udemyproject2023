package com.Basic;

import SpotifyApiTesting.utils.ConfigLoader;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
public class JsonSchemaValidation {

    /*
    JSON Schema is a declarative language that you can use to annotate
    and validate the structure, constraints, and data types of your JSON documents.
    It provides a way to standardize and define expectations for your JSON data.
     */

    @Test
    public void verifyJsonSchemaPositiveCase(){
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .when().get("/workspaces")
                .then().log().all()
                .assertThat().statusCode(200)
                .body(matchesJsonSchemaInClasspath("jsonValidationFilePositive.json"));
    }
@Test
    public void verifyJsonSchemaNegativeCase(){
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .when().get("/workspaces")
                .then().log().all()
                .assertThat().statusCode(200)
                .body(matchesJsonSchemaInClasspath("jsonValidationFileNegative.json"));
    }
}
