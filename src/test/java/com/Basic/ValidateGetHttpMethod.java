package com.Basic;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers.*;
import org.testng.annotations.Test;
public class ValidateGetHttpMethod {

    @Test
    public void getAllWorkspaces1() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .log().all()
                .when().get("/workspaces")
                .then().log().all()
                .assertThat().statusCode(200);
    }

    @Test
    public void getSingleWorkspaces() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .when().get("/workspaces")
                .then().log().all()
                .statusCode(200)
                .body("workspaces.name", hasItems("workspaceByNestedPojo3","workspace5"));
               //body("workspaces[2].name", equalTo("Workspace1"), "workspaces.size()", equalTo(5));


    }

    @Test
    public void extractResponse() {
        Response response = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when().get("/workspaces").then()
                .extract().response();

        System.out.println("header  received :" + response.getHeaders());
        System.out.println("-------------------------------------------");
        System.out.println("seesion id  received :" + response.getSessionId());
        System.out.println("-------------------------------------------");
        System.out.println("content type  received :" + response.contentType());
        System.out.println("-------------------------------------------");
        System.out.println("time  received :" + response.getTime());




    }

    @Test
    public void extractSingleValueFromResponse() {
        Response response = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when().get("/workspaces").then()
                .extract().response();
        System.out.println("worspace number 2 :" + response.path("workspaces[2].name"));// way 1 two retrive single value
        JsonPath jsonPath = new JsonPath(response.asString());
        String OutputWorskpace = jsonPath.getString("workspaces[2].name");// way 2 to retrive single value from json body
        System.out.println("workspace output: " + OutputWorskpace);


    }


}
