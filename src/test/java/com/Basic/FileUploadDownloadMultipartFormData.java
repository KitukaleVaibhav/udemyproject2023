package com.Basic;

import org.testng.annotations.Test;

import java.io.*;

import static io.restassured.RestAssured.*;

public class FileUploadDownloadMultipartFormData {

    @Test
    public void fileUpload() { // file upload with multipart form data
        given().baseUri("https://postman-echo.com")
                .multiPart("name","Rakesh")
                .multiPart("file", new File("src/test/resources/ApiFileUpload"))
                .log().all()
                .when()
                .post("/post")
                .then().log().all();
    }

    @Test
    public void fileDownload() throws IOException {
        byte[] bytes=given().baseUri("https://github.com/").when()
                .get("appium/appium/raw/master/packages/appium/sample-code/apps/ApiDemos-debug.apk")
                .then().extract().response().asByteArray();

        OutputStream os=new FileOutputStream(new File("ApiDemos-debug.apk"));
        os.write(bytes);
        os.close();

    }

}
