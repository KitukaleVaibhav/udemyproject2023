package com.Basic;


import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.Method;
import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import io.restassured.specification.QueryableRequestSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.SpecificationQuerier;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.equalTo;

public class RequestSpecificationUse {
     // RequestSpecification requestSpecification;


      //@BeforeClass
    public void beforeClass() {
        requestSpecification = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key")).log().all();
        // here we use this code becausse it is same for all method

    }

    //@BeforeClass
    public void RequestSpecBuilderClassUse() {

        /*
        The RequestSpecBuilder class is a part of the RestAssured library in Java. It is used to
        build a request specification that can be used to configure the request parameters such
         as headers, query parameters, and body content.The RequestSpecBuilder class provides a
         fluent API that allows you to chain multiple methods to set the request parameters.
         For example, you can use the addHeader() method to add a header to the request,
          the addQueryParameter() method to add a query parameter, and the setBody() method to set
          the request body.Here is an example of how to use the RequestSpecBuilder class to set the
          request parameters:
         */
        System.out.println("request spcification builder class use type1 method RequestSpecBuilderClassUse");
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"));
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();// if we use the build the it'Root1 mandatory to use  given().spec(requestSpecification); or given(requestSpecification)


    }

    @Test
    public void validateStatusCode() {
        System.out.println("validate status code method running");
        given(requestSpecification).
                when().get("/workspaces").
                then().log().all().
                assertThat().statusCode(200);

    }

    @Test
    public void validateStatusLine() {
        given().spec(requestSpecification).
                when().get("/workspaces").
                then().log().all().
                assertThat().statusLine("HTTP/1.1 200 ");

    }

    @Test
    public void bdd_To_nonBdd() {
        Response response = requestSpecification.request(Method.GET,"/workspaces");
        assertThat(response.path("workspaces[0].name"), equalTo("Team Workspace"));

    }


    @BeforeClass
    public void defaultSpecification() {
        System.out.println("defaultSpecification method of call to create requestSpecification");
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"));
        requestSpecBuilder.log(LogDetail.ALL);
        RestAssured.requestSpecification = requestSpecBuilder.build();
        /* here what happening the generated request specification object is storing in static variable of requestSpecification whose type is RequestSpecification
         so when we create this because this is static variable we can directly use requestSpecification variable
          // if we use the build the it'Root1 mandatory to use  given().spec(requestSpecification); or given(requestSpecification)
       */

    }

    @Test
    public void defaultSpecificationWorkingCheck() {
        request(Method.GET, "/workspaces").then().log().all();
    }

      @Test()
    public void queryRequestspecification() {
        QueryableRequestSpecification qRequestSpecification = SpecificationQuerier.query(requestSpecification);
        String baseUriReceiveByQuery = qRequestSpecification.getBaseUri();
          System.out.println("baseUriReceiveByQuery " + baseUriReceiveByQuery);
        System.out.println("Method Name:"+qRequestSpecification.getHeaders());


    }

    public void specificationWithChaining() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com")
                .addHeader("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .log(LogDetail.ALL);
        RestAssured.requestSpecification = requestSpecBuilder.build();
    }

}
