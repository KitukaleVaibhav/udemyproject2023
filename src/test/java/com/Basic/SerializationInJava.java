package com.Basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SerializationInJava {


    @Test
    public void serialize() throws IOException {
        FileOutputStream fo = new FileOutputStream(new File("serialiaztion.json"));
        HashMap<String, Object> mainObject = new HashMap<>();
        HashMap<String, String> nestedObject = new HashMap<>();
        nestedObject.put("name", "myUpdatedWorkspace");
        nestedObject.put("type", "personal");
        nestedObject.put("decription", "Reast assured testing");
        mainObject.put("workspace", nestedObject);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(fo, mainObject);
        String payload=objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mainObject);
        System.out.println(payload);
        String mainPayloadstr = objectMapper.writeValueAsString(mainObject);

    }

    @Test
    public void serializeList() throws JsonProcessingException {

        List<Object> mainList = new ArrayList<>();
        HashMap<String, Object> personal = new HashMap<>();
        personal.put("name1", "Vaibhav");
        personal.put("age", 27);
        HashMap<String, Object> professional = new HashMap<>();
        professional.put("companyId", 10);
        professional.put("companyName", "deutch");

        HashMap<String, Object> userCred = new HashMap<>();
        userCred.put("personal", personal);
        userCred.put("professional", professional);
        // mainList.add(userCred);
        mainList.add(userCred);

        /*l1.add("user")

        HashMap<String,Object> h2=new HashMap<>();
        h2.put("name2","user2");
        h2.put("age",70);
        l1.add(h1);
        l1.add(h2);*/
        //ObjectMapper objectMapper=new ObjectMapper();
        ObjectWriter objectMapper = new ObjectMapper().writerWithDefaultPrettyPrinter();// we use this to show response in good format
        String payload = objectMapper.writeValueAsString(mainList);
        System.out.println(payload);

    }
}



