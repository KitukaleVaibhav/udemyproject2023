package com.Basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ObjectMapperUse {
    @Test
    public void serializeList() throws JsonProcessingException {

        List<Object> mainList = new ArrayList<>();
        HashMap<String, Object> personal = new HashMap<>();
        personal.put("name1", "Vaibhav");
        personal.put("age", 27);
        HashMap<String, Object> professional = new HashMap<>();
        professional.put("companyId", 10);
        professional.put("companyName", "deutch");

        HashMap<String, Object> userCred = new HashMap<>();
        userCred.put("personal", personal);
        userCred.put("professional", professional);



   ObjectMapper objectMapper=new ObjectMapper();
  ObjectNode objectNode =objectMapper.createObjectNode();
  objectNode.putArray("array");
  objectNode.put("name","user1");



    }
}
