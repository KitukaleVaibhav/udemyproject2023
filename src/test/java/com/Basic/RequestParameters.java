package com.Basic;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class RequestParameters {


    @Test
    public void single_Query_ParameterPass(){
        given().baseUri("https://postman-echo.com/")
                .queryParam("foo1","bar1")   //param("foo1","bar1") we can use queryParam() or param() method to pass parameter
                .when().get("/get")
                .then().log().all();
    }


    @Test
    public void send_MultipleQueryParameters(){
        given().baseUri("https://postman-echo.com/")
                .queryParam("foo1","bar1")
                .queryParam("foo2","bar2").log().all()
                .when().get("/get")
                .then().log().all();
    }

    @Test
    public void multivalue_Query_Parameter(){
        given().baseUri("https://postman-echo.com/")
                .queryParam("foo1","bar1","bar2","bar3").log().all()
                .when().get("/get")
                .then().log().all();
    }
}
