package com.Basic;

import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class ResponseSpecificationUse {
    /*
The `ResponseSpecification` class is also a part of the RestAssured library in Java and is used to specify the expected response from the server.
 It is used in conjunction with the `RequestSpecification` class to specify the request parameters and expected response parameters.
The `ResponseSpecification` class provides a fluent API that allows you to chain multiple methods to set the expected response parameters.
For example, you can use the `statusCode()` method to specify the expected HTTP status code, the `body()` method to specify the expected response body,
and the `header()` method to specify the expected response headers.

Here is an example of how to use the `ResponseSpecification` class to specify the expected response:

```
ResponseSpecification responseSpec = new ResponseSpecBuilder()
   .expectStatusCode(200)
   .expectBody("name", equalTo("John Doe"))
   .expectHeader("Content-Type", equalTo("application/json"))
   .build();
```
In this example, we are specifying that the expected HTTP status code is 200,
 the expected response body contains a "name" field with a value of "John Doe",
 and the expected response headers contain a "Content-Type" field with a value of "application/json".
Once you have built the response specification, you can use it to verify the response from the server using the
RestAssured library. The `Response` object returned by the server can be verified against the expected response
parameters using the methods provided by the `ResponseSpecification` class.
     */

    ResponseSpecification responseSpecification;
    RequestSpecification requestSpecification;


  //  @BeforeClass
    public void generalResponse(){
        System.out.println("negenarl response method call");
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"));
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();
       responseSpecification =RestAssured.expect().statusCode(200)
                .statusLine("HTTP/1.1 200 ");
    }
  @BeforeClass
    public void setup() {
      System.out.println("setup method call");
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"));
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();

        // responseSpecBuilder class using
        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .log(LogDetail.ALL)
                .expectStatusLine("HTTP/1.1 200 ");
        responseSpecification = responseSpecBuilder.build();
        // we use this ResponseSpecification to reduce code reusibility.
    }

    @Test
    public void validateResponse() {
        given().spec(requestSpecification).when().
        get("/workspaces")
                .then().spec(responseSpecification);
    }

   // @BeforeClass
    public void responseSpecBuilderClassUse() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.getpostman.com");
        requestSpecBuilder.addHeader("x-api-key",ConfigLoader.getInstance().getPropertyValue("api-key") );
        RestAssured.requestSpecification = requestSpecBuilder.build();
        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .expectStatusLine("HTTP/1.1 200 ");
      RestAssured.responseSpecification = responseSpecBuilder.build();
    }

    @Test
    public void testWithResponseSpecBuilderClass() {
      given().spec(requestSpecification).when().get("/workspaces")
                .then().spec(responseSpecification).log().all();
    }

    @Test
    public void nonbdd(){
        given().spec(requestSpecification).request(Method.GET,"/workspaces").then().spec(responseSpecification);
    }
}
