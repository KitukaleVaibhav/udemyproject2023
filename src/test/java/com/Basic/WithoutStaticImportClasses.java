package com.Basic;

import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class WithoutStaticImportClasses {
    @Test
    public void withoutStaticImport() {
        RestAssured.given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .when()
                .get("/workspaces")
                .then().log().all();
    }

}
