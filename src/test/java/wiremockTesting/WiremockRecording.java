package wiremockTesting;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import io.restassured.response.Response;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static io.restassured.RestAssured.given;

public class WiremockRecording {
    private WireMockServer wireMockServer;

    @BeforeMethod
    public void setup() {
        wireMockServer = new WireMockServer(options().port(3030));
        wireMockServer.start();

        // Configure WireMock to record interactions with the external API
        RecordSpecBuilder recordSpec = WireMock.recordSpec()
                .forTarget("https://reqres.in")  // Replace with your actual API URL
                .makeStubsPersistent(true);
        wireMockServer.startRecording(recordSpec);


    }

    @Test
    public void testMain() {
        utilTestGetUser();
        SnapshotRecordResult snapshotRecordResult = wireMockServer.stopRecording();
        List<StubMapping> sb = snapshotRecordResult.getStubMappings();
    }

    public static void utilTestGetUser() {
        Response response = given().baseUri("http://localhost:3030")
                .when()
                .get("/api/users/")
                .then().extract().response();
        System.out.println(response.prettyPrint());
    }

    public static void getRequiredThingsFromRealApi() {
        Response response = given().baseUri("http://localhost:3030")
                .when()
                .get("/api/users/")
                .then().extract().response();
        System.out.println(response.prettyPrint());
    }
    //@AfterMethod
    public void setDown(){
        wireMockServer.stop();
    }
}
