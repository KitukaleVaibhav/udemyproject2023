package wiremockTesting;

import SpotifyApiTesting.utils.ConfigLoader;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.testng.annotations.Test;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static io.restassured.RestAssured.given;

public class PlayRecordedStub {
    @Test
    public void testRecorded(){
        String rootDir = "src/test/resources";

        // Create a WireMock configuration pointing to the root directory
        WireMockConfiguration config = WireMockConfiguration.wireMockConfig()
                .port(3030)  // Define the port where WireMock will run
                .withRootDirectory(rootDir);
        WireMockServer wireMockServer = new WireMockServer(config);
        wireMockServer.start();

        // Optional: If you want to add additional behavior
        WireMock.configureFor("localhost", 3030);
        // WireMock.stubFor(...); // Define additional stubs if needed

        // Keep the server running until manually stopped or a condition is met
        System.out.println("WireMock server is running with recorded stubs...");
        given().baseUri("http://localhost:3030")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key")).log().headers()
                .when().get("/workspaces")
                .then().log().status()
                .assertThat().statusCode(200);
        // Stop WireMock when done
        Runtime.getRuntime().addShutdownHook(new Thread(wireMockServer::stop));
    }
    }

