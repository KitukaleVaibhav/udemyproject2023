package wiremockTesting;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockRecorder2 {
    public static void main(String[] args) {

            // Start WireMock server
            WireMockServer wireMockServer = new WireMockServer(options().port(3030));
            wireMockServer.start();

            // Configure WireMock to record interactions with the external API
            RecordSpecBuilder recordSpec = WireMock.recordSpec()
                    .forTarget("https://reqres.in")  // Replace with your actual API URL
                    .makeStubsPersistent(true);     // Save stubs for future use

            // Start recording
            wireMockServer.startRecording(recordSpec);

            // REST Assured request to the WireMock proxy, which will follow redirects
            RestAssured.baseURI = "http://localhost:3030";
            Response response = RestAssured
                    .given()
                    .when()
                    .get("/api/users/2")  // Target the endpoint that may cause a 301 redirect
                    .then()
                    .statusCode(200)  // Expecting the final successful status code
                    .extract()
                    .response();

            // Log the final response (optional)
            System.out.println("Response Body: " + response.getBody().asString());

            // Stop recording and save the stubs
            wireMockServer.stopRecording();

            // Shutdown the WireMock server
            wireMockServer.stop();
        }
    }



