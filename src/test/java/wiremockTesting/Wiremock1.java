package wiremockTesting;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static io.restassured.RestAssured.given;


public class Wiremock1 {
    private WireMockServer wireMockServer;

    @BeforeMethod
    public void setup() {
        // Start the WireMock server on port 8080
        wireMockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().port(3030));
        wireMockServer.start();

        // Configure WireMock to stub a POST request
        wireMockServer.stubFor(post(urlEqualTo("/api/test"))
                .withHeader("Authorization", equalTo("Bearer test-token"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withHeader("X-Custom-Header", "CustomHeaderValue")
                        .withBody("{\"status\":\"success\"}")));


    }

    @AfterMethod
    public void tearDown() {
        // Stop the WireMock server
        wireMockServer.stop();
    }


    @Test
    public void testServiceWithRightCredentials() {
        given().baseUri("http://localhost:3030/api")
                .header("Authorization", "Bearer test-token")
                .when().post("/test")
                .then().log().all();
    }

    @Test
    public void testServiceWithWrongCredentials() {
        given().baseUri("http://localhost:3030/api")
                .when().post("/test")
                .then().log().all();
    }

    @Test
    public void testOuth2WithRightData() {

    }

}
