package wiremockTesting;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import wiremockTesting.WiremockUtility;

import static io.restassured.RestAssured.given;


public class Wiremock2 {
    private WireMockServer wireMockServer;

    @BeforeMethod
    public void setup() {
        // Start the WireMock server on port 8080
        wireMockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().port(3030));
        wireMockServer.start();

        // Configure WireMock to stub a POST request
        // wireMockServer.stubFor();
    }

    @Test
    public void checkLoginApiWithCorrectCredentials() {
        WiremockUtility.checkCorrectAuthenticationData(wireMockServer);
        given().header("Authorization", "Bearer test-token")
                .baseUri("http://localhost:3030/")
                .request().log().all()
                .when().post("api/test")
                .then().log().all();
    }

    @Test
    public void checkLoginApiWithWrongCredentials() {
        WiremockUtility.checkWrongAuthenticationData(wireMockServer);
        WiremockUtility.checkCorrectAuthenticationData(wireMockServer);
        given()
                .baseUri("http://localhost:3030/")
                .request().log().all()
                .when().post("api/test")
                .then().log().all();
    }

    @Test
    public void testOuth2WithRightData() {
        WiremockUtility.checkOuthFlowWithCorrectCredentials(wireMockServer);
        given()
                .baseUri("http://localhost:3030/")
                .formParam("client_id", "my-client-id")
                .formParam("client_secret", "my-client-secret")
                .formParam("grant_type", "password")
                .formParam("username", "my-username")
                .formParam("password", "my-password").log().all()
                .when().post("/token")
                .then().log().all();
    }

    @Test
    public void testOuthWithWrongdata() {
        WiremockUtility.checkOuthFlowWithIncorrectCredentials(wireMockServer);
        given()
                .baseUri("http://localhost:3030/")
                .formParam("client_id", "wrong-client-id")
                .formParam("client_secret", "wrong-client-secret")
                .formParam("grant_type", "password")
                .formParam("username", "my-username")
                .formParam("password", "password").log().all()
                .when().post("/token")
                .then().log().all();
    }

    @Test
    public void schemaCheck(){
        WiremockUtility.checkRequestBodyJsonSchema(wireMockServer);
        given()
                .baseUri("http://localhost:3030")
                .body(WiremockUtility.requestBody()).then().log().all()
                .when().post("/creatUserProfile")
                .then().log().all();

    }
    @AfterMethod
    public void tearDown() {
        // Stop the WireMock server
        wireMockServer.stop();
    }


}

