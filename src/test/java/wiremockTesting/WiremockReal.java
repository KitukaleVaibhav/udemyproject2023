package wiremockTesting;

import SpotifyApiTesting.utils.ConfigLoader;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static io.restassured.RestAssured.given;

public class WiremockReal {

    private WireMockServer wireMockServer;

    @BeforeMethod
    public void setup() {
        wireMockServer = new WireMockServer(options().port(3030));
        wireMockServer.start();

        // Configure WireMock to record interactions with the external API
        RecordSpecBuilder recordSpec = WireMock.recordSpec()
                .forTarget("https://api.getpostman.com")
                .captureHeader("x-api-key")// Replace with your actual API URL
                .makeStubsPersistent(true);
        wireMockServer.startRecording(recordSpec);


    }

    @Test
    public void getWorkspace() {
        getAllWorkspaces1();
        SnapshotRecordResult snapshotRecordResult = wireMockServer.stopRecording();
    }


    public void getAllWorkspaces1() {
        given().baseUri("http://localhost:3030")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key")).log().headers()
                .when().get("/workspaces")
                .then().log().all()
                .assertThat().statusCode(200);
    }


}
