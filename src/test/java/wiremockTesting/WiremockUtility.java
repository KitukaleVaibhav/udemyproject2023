package wiremockTesting;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

public class WiremockUtility {

    public static void checkWrongAuthenticationData(WireMockServer wireMock) {
        wireMock.stubFor(post(urlEqualTo("/api/test"))
                .willReturn(aResponse()
                        .withStatus(401)
                        .withHeader("Content-Type", "application/json")
                        .withHeader("X-Custom-Header", "CustomHeaderValue")
                        .withBody("{\"status\":\"unauthorized\"}")));
    }

    public static void checkCorrectAuthenticationData(WireMockServer wireMock) {
        wireMock.stubFor(post(urlEqualTo("/api/test"))
                .withHeader("Authorization", equalTo("Bearer test-token"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withHeader("X-Custom-Header", "CustomHeaderValue")
                        .withBody("{\"status\":\"success\"}")));
    }

    public static void checkOuthFlowWithCorrectCredentials(WireMockServer wireMock) {
        wireMock.stubFor(post(urlEqualTo("/token"))
                .withRequestBody(equalTo("client_id=my-client-id&client_secret=my-client-secret&grant_type=password&username=my-username&password=my-password"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{ \"access_token\": \"your-access-token\", \"token_type\": \"Bearer\", \"expires_in\": 3600 }")));

    }

    public static void checkOuthFlowWithIncorrectCredentials(WireMockServer wireMock) {
        wireMock.stubFor(post(urlEqualTo("/token"))
                .withRequestBody(matching(
                        "^client_id=wrong-client-id&client_secret=wrong-client-secret&grant_type=password&username=.*&password=password$"
                ))
                        /*withRequestBody(matching("^client_id=wrong-client-id$"))
                .withRequestBody(matching("client_secret=wrong-client-secret$"))
                .withRequestBody(matching("^grant_type=password$"))
                .withRequestBody(matching(".*username=.*"))
                .withRequestBody(notMatching("^password=my-password$"))*/

                .willReturn(aResponse()
                        .withStatus(401)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{ \"error\": \"invalid_grant\", \"error_description\": \"Invalid credentials\" }")));
    }

    public static void checkRequestBodyJsonSchema(WireMockServer wireMockServer){
        wireMockServer.stubFor(post(urlEqualTo("creatUserProfile"))
                .withRequestBody(matchingJsonSchema(schema()))
                .withRequestBody(equalTo(requestBody()))
                .willReturn(aResponse()
                        .withBody("{success}")
                        .withStatus(201)
                        .withStatusMessage("created")
                )
        );
    }


    public static String schema(){
        String s="{\n" +
                "  \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n" +
                "  \"title\": \"UserProfile\",\n" +
                "  \"type\": \"object\",\n" +
                "  \"properties\": {\n" +
                "    \"personalInformation\": {\n" +
                "      \"type\": \"object\",\n" +
                "      \"properties\": {\n" +
                "        \"firstName\": { \"type\": \"string\" },\n" +
                "        \"lastName\": { \"type\": \"string\" },\n" +
                "        \"email\": { \"type\": \"string\", \"format\": \"email\" },\n" +
                "        \"dateOfBirth\": { \"type\": \"string\", \"format\": \"date\"   \n" +
                " },\n" +
                "        \"gender\": { \"type\": \"string\" },\n" +
                "        \"phoneNumber\": { \"type\": \"string\" }\n" +
                "      },\n" +
                "      \"required\": [\"firstName\", \"lastName\", \"email\", \"dateOfBirth\"]\n" +
                "    },\n" +
                "    \"contactInformation\": {\n" +
                "      \"type\": \"object\",\n" +
                "      \"properties\": {\n" +
                "        \"address\": { \"type\": \"string\" },\n" +
                "        \"city\": { \"type\": \"string\" },\n" +
                "        \"stateProvince\": { \"type\": \"string\" },\n" +
                "        \"country\": { \"type\": \"string\" },\n" +
                "        \"postalCode\": { \"type\": \"string\" }\n" +
                "      },\n" +
                "      \"required\": [\"address\", \"city\", \"country\"]\n" +
                "    },\n" +
                "    \"socialMedia\": {\n" +
                "      \"type\": \"object\",\n" +
                "      \"properties\": {\n" +
                "        \"facebookUrl\": { \"type\": \"string\" },\n" +
                "        \"twitterUrl\": { \"type\": \"string\" },\n" +
                "        \"linkedinUrl\": { \"type\": \"string\" }\n" +
                "      }\n" +
                "    },\n" +
                "    \"preferences\": {\n" +
                "      \"type\": \"object\",\n" +
                "      \"properties\": {\n" +
                "        \"newsletterSubscription\": { \"type\": \"boolean\" },\n" +
                "        \"languagePreference\": { \"type\": \"string\" },\n" +
                "        \"themePreference\": { \"type\": \"string\" }\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"required\": [\"personalInformation\", \"contactInformation\"]\n" +
                "}";
        return s;
    }

    public static String requestBody(){
        String request="{\n" +
                "  \"personalInformation\": {\n" +
                "    \"firstName\": \"rakesh\" ,\n" +
                "    \"lastName\": \"Doe\",\n" +
                "    \"email\": \"johndoe@example.com\",\n" +
                "    \"dateOfBirth\": \"1990-01-01\",\n" +
                "    \"gender\": \"Male\",\n" +
                "    \"phoneNumber\": \"+1234567890\"\n" +
                "  },\n" +
                "  \"contactInformation\": {\n" +
                "    \"address\": \"123 Main Street\",\n" +
                "    \"city\": \"Anytown\",\n" +
                "    \"stateProvince\": \"CA\",\n" +
                "    \"country\": \"USA\",\n" +
                "    \"postalCode\": \"12345\"\n" +
                "  },\n" +
                "  \"socialMedia\": {\n" +
                "    \"facebookUrl\": \"https://facebook.com/johndoe\",\n" +
                "    \"twitterUrl\": \"https://twitter.com/johndoe\"\n" +
                "  },\n" +
                "  \"preferences\": {\n" +
                "    \"newsletterSubscription\": true,\n" +
                "    \"languagePreference\": \"English\",\n" +
                "    \"themePreference\": \"Dark\"\n" +
                "  }\n" +
                "}";
        return request;
    }

}
