package wiremockTesting;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import interviewQuestions.PaginationBody;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

public class WireMockUtilityAdvancedScenario {

    public static void returnProductDetailsViaJsonFileResponse(WireMockServer wireMockServer) throws IOException {
        File file = new File("src/test/resources/responseBody.json");
        String content = new String(Files.readAllBytes(Paths.get("src/test/resources/responseBody.json")));
        wireMockServer.stubFor(get(urlEqualTo("/productOffering"))
                .withHeader("Authorization", equalTo("Bearer 1234"))
                .willReturn(aResponse()
                        .withHeader("server", "nginx")
                        .withHeader("contentType", "application/json")
                        .withStatus(200)
                        .withStatusMessage("Ok")
                        .withBody(content)

                )
        );
    }


    public static void queryparamApiWithPagination(WireMockServer wireMockServer, int pageNo, int PageSize) {
        String content = PaginationBody.paginationdataGet(pageNo, PageSize);
        wireMockServer.stubFor(get(urlPathEqualTo("/api/resource"))
                .withQueryParam("Page", equalTo(String.valueOf(pageNo)))
                .withQueryParam("Size", equalTo(String.valueOf(PageSize)))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody(content)));

    }

    public static void internalServerErrorTesting(WireMockServer wireMockServer) {
        wireMockServer.stubFor(get(urlEqualTo("/api/server-error"))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withBody("{ \"error\": \"Internal server error\" }")
                        .withHeader("Content-Type", "application/json")));
    }

    public static void apiWorkingSlowly(WireMockServer wireMockServer) {
        stubFor(get(urlEqualTo("/api/delayed-response"))
                .willReturn(aResponse()
                        .withFixedDelay(5000)  // 5-second delay
                        .withStatus(200)
                        .withBody("{ \"message\": \"This response was delayed\" }")));
    }


    public static void setupMultipartStub(WireMockServer wireMockServer) {
        wireMockServer.stubFor(post(urlEqualTo("/upload"))
                .withMultipartRequestBody(aMultipart()
                        .withName("file")
                        .withBody(equalTo("File upload testing."))  // Match any file content
                )
                .withMultipartRequestBody(aMultipart()
                        .withName("description")
                        .withBody(equalTo("Test file upload"))
                )
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("{ \"message\": \"File uploaded successfully\" }")
                        .withHeader("Content-Type", "application/json")));
    }

    public static void FormParameter2Test(WireMockServer wireMockServer) {
        //wireMockServer.stubFor(g)
        wireMockServer.stubFor(post(urlEqualTo("/login"))
                .withFormParam("username", matching("^[a-zA-Z]{1,15}$"))
                .withFormParam("password", matching("^password.*"))
                .withCookie("jsessionId", matching("^[a-zA-Z0-9]{15}$"))
                .willReturn(aResponse().withStatus(200)
                        .withStatusMessage("success")
                        .withBody("login succesfully"))
        );
    }

    public static void postWithHeavyPayload(WireMockServer wireMockServer){
        wireMockServer.stubFor(post(urlEqualTo("/prouctCreat"))
                .willReturn(aResponse().withStatus(200)
                        .withStatusMessage("success")
                        .withBody("login succesfully"))
        );
    }
}

