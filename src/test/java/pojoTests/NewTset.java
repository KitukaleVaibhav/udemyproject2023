package pojoTests;

import org.testng.annotations.Test;
import pojoClasses.Workspace;
import pojoClasses.WorkspaceRoot;

import static io.restassured.RestAssured.given;

public class NewTset {
    @Test
    public void test(){
        Workspace workspace = new Workspace("workspaceByNestedPojo", "personal", "it'Root1 created on 22/11/2023");
        WorkspaceRoot workspaceRoot = new WorkspaceRoot(workspace);
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .body(workspaceRoot)
                .log().all()
                .when().post("/workspaces")
                .then().log().all();
    }
}
