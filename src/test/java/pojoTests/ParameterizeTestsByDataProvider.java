package pojoTests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pojoClasses.Workspace;
import pojoClasses.WorkspaceRoot;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ParameterizeTestsByDataProvider {

    @Test(dataProvider = "workspace")
    public void create_Multiple_workspaceBy_usingDataprovider(String name,String type,String desciption) {
        Workspace workspace = new Workspace(name,type,desciption);
        WorkspaceRoot workspaceRoot = new WorkspaceRoot(workspace);
        WorkspaceRoot deserializeWorskspaceRoot = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .body(workspaceRoot).log().all()
                .when().post("/workspaces")
                .then().extract().response().as(WorkspaceRoot.class);
        assertThat(deserializeWorskspaceRoot.getWorkspace().getName(), equalTo(name));
        String id = deserializeWorskspaceRoot.getWorkspace().getId().toString();
        System.out.println("id pickup: " + id);
    }

    @DataProvider(name = "workspace")
    public Object[][] passPojoPayload() {

        Object[][] workspacePayload = {{"workspace5", "personal", "created workspace5"}, {"workspace6", "personal", "created workspace6"}, {"workspace7", "team", "created workspace7"}};
        return workspacePayload;
    }
}

