package pojoTests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.annotations.Test;
import pojoClasses.SimplePojo;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class BasicTestWithSimplePojo {

    @Test
    public void TestSimplePojo() throws JsonProcessingException {
        SimplePojo simplePojo=new SimplePojo("Value1","Value2");
        given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io")
                .body(simplePojo) // internally rest assured used jackson to serailize pojo object into json object we don't need to do anything exeternally.
                .when().post("/SimplePojoTest")
                .then().log().all().assertThat().body("key1", equalTo(simplePojo.getKey1()),
                "key2",equalTo(simplePojo.getKey2())).log().all();
    }
    @Test
    public void desrializeByPojo2(){
        SimplePojo simplePojo=new SimplePojo("value1","value2");
      SimplePojo sp = given().baseUri("https://11ad500d-9dda-497c-a086-9842751287fc.mock.pstmn.io")
                .body(simplePojo) // internally rest assured used jackson to serailize pojo object into json object we don't need to do anything exeternally.
                .when().post("/SimplePojoTest")
                .then().extract().response().as(SimplePojo.class);

      System.out.println(sp.getKey1());
    }
}
