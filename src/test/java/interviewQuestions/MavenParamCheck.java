package interviewQuestions;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MavenParamCheck {
    @Test
    @Parameters({"Name", "id", "age"})
    public void main(String Name, int id, int age) {
        System.out.println(Name);
        System.out.println(id);
        System.out.println(age);
    }
}
