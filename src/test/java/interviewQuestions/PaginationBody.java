package interviewQuestions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.jayway.jsonpath.JsonPath.*;

public class PaginationBody {
    public static String paginationdataGet(int pageNo,int itemSize){
        String filepath="src/test/resources/page_"+pageNo+".json";
        String content = null;
        try {
            content = new String(Files.readAllBytes(Paths.get(filepath)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // System.out.println(content);
        String pathObjectList="$.data[:"+itemSize+"]";
        List<Object> l1= read(content,pathObjectList);
        int page=JsonPath.read(content,"$.page");
        int per_page=JsonPath.read(content,"$.per_page");
        int total=JsonPath.read(content,"$.total");
        int total_pages=JsonPath.read(content,"$.total_pages");
        System.out.println("pageName:"+page);
        PaginationPojo pjo=new PaginationPojo(page,per_page,total,total_pages,l1);
        ObjectMapper mapper=new ObjectMapper();
        String data = null;
        try {
            data = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pjo);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return data;
    }
}
