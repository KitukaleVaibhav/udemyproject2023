package interviewQuestions;

public enum EnumDescription2 {
        ADDITION {
            @Override
            public double apply(double x, double y) {
                return x + y;
            }
        },
        SUBTRACTION {
            @Override
            public double apply(double x, double y) {
                return x - y;
            }
        },
        MULTIPLICATION {
            @Override
            public double apply(double x, double y) {
                return x * y;
            }
        },
        DIVISION {
            @Override
            public double apply(double x, double y) {
                return x / y;
            }
        };

        // Abstract method that each constant must implement
        public abstract double apply(double x, double y);
    }




