package interviewQuestions;

import io.jsonwebtoken.*;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

public class JwtRecreation {

        private static final String SECRET_KEY = "your-secret-key";

        // Method to create a token (for testing purposes)
        public static String createToken(String userId) {
            return Jwts.builder()
                    .setSubject(userId)
                    .setIssuer("your-app")
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(System.currentTimeMillis() + 3600000)) // 1 hour expiration
                    .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                    .compact();
        }

        // Method to manually recalculate and verify the signature of the token
        public static boolean validateTokenSignature(String token) {
            try {
                System.out.println("inside try block");
                // Split the token into its components
                String[] parts = token.split("\\.");
                if (parts.length != 3) {
                    throw new IllegalArgumentException("Invalid JWT token format.");
                }

                String header = parts[0];
                String payload = parts[1];
                String receivedSignature = parts[2];

                // Recreate the signature
                String data = header + "." + payload;
                System.out.println("data:"+data);
                String recalculatedSignature = calculateHMAC(data, SECRET_KEY);
                System.out.println("recalculated signauture: "+recalculatedSignature);
                System.out.println("received signature: "+receivedSignature);
                // Compare the recalculated signature with the received signature
                if (!recalculatedSignature.equals(receivedSignature)) {
                    System.out.println("Invalid token signature.");
                    return false;
                }

                // If we reach here, the signature is valid, now parse the claims
                Jws<Claims> claims = Jwts.parser()
                        .setSigningKey(SECRET_KEY)
                        .parseClaimsJws(token);

                System.out.println("Signature is valid.");
                System.out.println("User ID: " + claims.getBody().getSubject());
                return true;
            } catch (SignatureException e) {
                System.out.println("Inside catch block");
                System.out.println("Invalid token signature.");
            } catch (Exception e) {
                System.out.println("Invalid token: " + e.getMessage());
            }

            return false;
        }

        // Helper method to calculate HMAC SHA-256 signature
        private static String calculateHMAC(String data, String key) throws Exception {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            mac.init(secretKeySpec);
            byte[] rawHmac = mac.doFinal(data.getBytes());

            // Encode the byte array to Base64Url
            return Base64.encodeBase64URLSafeString(rawHmac);
        }

        public static void main(String[] args) {
            // Create a token for testing
            String token = createToken("12345");

            // Print the token
            System.out.println("Generated Token: " + token);

            // Validate the token signature
            boolean isValid = validateTokenSignature(token);
            System.out.println("Is token signature valid? " + isValid);
        }
    }


