package interviewQuestions;

import org.testng.annotations.Test;

import java.awt.*;
import java.io.*;
import java.util.Arrays;

public class Filepractise {
    @Test
    public void createFile() throws IOException {
        String fileFolder = "src/test/resources/FilesTest";
        String filePath = "src/test/resources/FilesTest/test2.txt";
        File ffolder = new File(fileFolder);
        File fPath = new File(filePath);
        fPath.createNewFile();
    }

    @Test
    public void deleteFiles() {
        String fileFolder = "allure-results";
        File file = new File(fileFolder);
        File[] files = file.listFiles();
        for (File f : files) {
            System.out.println(f.getName());
            f.setWritable(true);
            System.out.println(f.canRead()+" "+f.canWrite()+" "+f.canExecute());
           // f.delete();
        }

        //file.mkdir();
        //file.delete();
    }

    @Test
    public void readFile() throws IOException {
        String filename = "./resources/token.properties";
        FileReader fileReader = new FileReader(filename);
        int content;

        if ((content = fileReader.read()) != -1) {
            System.out.print((char) content);
        }

    }
}
