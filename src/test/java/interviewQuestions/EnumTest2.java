package interviewQuestions;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EnumTest2 {
    @Test
    public void test2() {
        double value = EnumDescription2.ADDITION.apply(0102, 39030);
        System.out.println(value);
        SubjectEnum english = SubjectEnum.ENGLISH;
        System.out.println(english.getValue());
        TimeUnit seconds = TimeUnit.SECONDS;
        long day = seconds.toDays(2900002000l);
        System.out.println(day);
    }
}

