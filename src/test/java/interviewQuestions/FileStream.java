package interviewQuestions;

import org.testng.annotations.Test;

import java.io.*;

public class FileStream {

    @Test
    public void inputCheck() throws IOException {
        String fileName = "src/test/resources/token.properties";
        FileInputStream fis = new FileInputStream(fileName);
        int content;
        while((content= fis.read())!=-1){
            System.out.print((char)content);

        }
    }
    @Test
    public void outputStreamCheck() throws IOException {
        String fileName = "src/test/resources/token.properties";
        FileOutputStream fis=new FileOutputStream(fileName);
        fis.write("idnjdm".getBytes());
        fis.flush();
    }
    @Test
    public void byteCode(){
        Integer name=190;
        System.out.println(name.byteValue());
        String code=Integer.toBinaryString(name);
        System.out.println(code);
    }
}
