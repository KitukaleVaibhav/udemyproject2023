package interviewQuestions;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.jsonwebtoken.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

public class TokenGenerationAndCreation {
    private static final String SECRET_KEY = "your-secret-key";
    private static final long EXPIRATION_TIME = 3600000; // 1 day

    // Generate JWT token
    @BeforeMethod()
    public static String generateToken() {
        return Jwts.builder()
                .setSubject("vaibhav")
                .setIssuedAt(new Date())
                .setAudience("tesijfno ms")
                .setIssuer("i am owner")
                .setHeaderParam("username", "vaibhav")
                .setHeaderParam("UserRole", "owner")
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public void ctoken() {

    }

    @Test
    public void generateAndCompareToken() {
        String token = generateToken();
        System.out.println("received token:" + token);
        Jws<Claims> s = Jwts.parser().setSigningKey("your-secret-key").parseClaimsJws(token);
        System.out.println("signature:" + s.getSignature());
        System.out.println("body:" + s.getBody());

        LinkedHashMap<String, Object> bodyClaim = new LinkedHashMap<>();
        Set<String> ids = s.getBody().keySet();
        for (String id : ids) {
            System.out.println(id + ": " + s.getBody().get(id));
            bodyClaim.put(id, s.getBody().get(id));
        }
        System.out.println("bodyStrore" + bodyClaim);
        LinkedHashMap<String, Object> headerClaim = new LinkedHashMap<String, Object>();
        System.out.println("-----------------------get header details--------------------------------------");
        Set<String> hkeys = s.getHeader().keySet();
        for (String k1 : hkeys) {
            System.out.println(k1 + ": " + s.getHeader().get(k1));
            headerClaim.put(k1, s.getHeader().get(k1));
        }


        String newToken = Jwts.builder().setHeader(headerClaim).
                setClaims(bodyClaim).
                signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();

        System.out.println("new token:" + newToken);

        Assert.assertEquals(token, newToken, "token validation checks");
    }

    public static String generateToken2() {
        return Jwts.builder()
                .setSubject("vaibhava")
                .setIssuedAt(new Date())
                .setAudience("tesijfno mss")
                .setIssuer("i am owner")
                .setHeaderParam("username", "vaibhav")
                .setHeaderParam("UserRole", "owner")
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    @Test
    public void checkJwtTokenValid() {
        String token1 = generateToken();
        String token2 = generateToken2();
        Assert.assertEquals(token1, token2);
        System.out.println("token1: " + generateToken());
        System.out.println("token2: " + generateToken2());

    }

    @Test
    public void printJwt() {
        System.out.println(generateToken());
        JWT jwt = new JWT();
        DecodedJWT decodedJWT = jwt.decodeJwt(generateToken());
        byte[] decodedBytes = Base64.getUrlDecoder().decode(decodedJWT.getPayload());
        String payload = new String(decodedBytes);
        System.out.println(" decoded payload:" + payload);
        byte[] sign = Base64.getUrlDecoder().decode("Ng38g6YJ8yFQv66saq6NcDiz_XPeB9BYGv3KywocQwU");
        String signCheck = new String(sign);
        System.out.println("sign" + signCheck);
        byte[] decodeSign = Base64.getDecoder().decode(decodedJWT.getSignature());
        String signature = new String(decodeSign);
        System.out.println("signature: " + signature);
    }

    // Parse and validate JWT token

    @Test(priority = 2)
    public static Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    // Check if token is expired
    @Test(priority = 3)
    public static boolean isTokenExpired(String token) {
        Claims claims = parseToken(token);
        return claims.getExpiration().before(new Date());
    }

}

