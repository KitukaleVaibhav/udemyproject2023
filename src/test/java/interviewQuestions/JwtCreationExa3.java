package interviewQuestions;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

public class JwtCreationExa3 {
        // Secret key for signing JWT (in real applications, keep this secure)
        private static final String SECRET_KEY = "your-secret-key";

        public static void main(String[] args) {
            // Existing JWT token
            String existingJwt = "your-existing-jwt-token";

            // Parse the JWT to extract the header and payload
            String header = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(existingJwt)
                    .getHeader()
                    .toString();

            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(existingJwt)
                    .getBody();

            // Print the extracted header and payload
            System.out.println("Header: " + header);
            System.out.println("Payload: " + claims);

            // Create a new JWT token with the same header and payload
            String newJwt = Jwts.builder()
                    //.setHeaderParams(TextCodec.BASE64.decodeToString(header))  // Decode and set header
                    .setClaims(claims)  // Set the same payload
                    .signWith(SignatureAlgorithm.HS256, SECRET_KEY)  // Sign the JWT
                    .compact();

            // Print the newly created JWT
            System.out.println("New JWT: " + newJwt);
        }
    }


