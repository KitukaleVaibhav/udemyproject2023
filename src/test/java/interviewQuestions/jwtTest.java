package interviewQuestions;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.impl.DefaultJwtBuilder;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Base64;

public class jwtTest {

    @BeforeMethod
    public static String test() {
        String idToken = RestAssured.given().baseUri("https://oauth2.googleapis.com")
                .formParam("client_id", "955301631655-bobg4cnlplv780nmap4fipb308rsmdqn.apps.googleusercontent.com")
                .formParam("client_secret", "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")
                .formParam("grant_type", "refresh_token")
                .formParam("refresh_token", "1//0g3k7Cp9IEplLCgYIARAAGBASNwF-L9IrS_879TbFQ_PeIoI8Y_lpLUv2jqD2cY4GbUAcQrpvNvDYzxlcyhJlQhA_KX7BgRj2Gyc")
                .when().post("/token").then().log().body().extract().path("id_token");

        System.out.println("id token rescived:" + idToken);
        return idToken;

    }
    @Test
 public void testJwt(){
     JWT jwt=new JWT();
     DecodedJWT decodedJWT=jwt.decodeJwt(test());

        // Print out the header, payload, and signature
        System.out.println("Header: " + decodedJWT.getHeader());
        System.out.println("Payload: " + decodedJWT.getPayload());
        System.out.println("Signature: " + decodedJWT.getSignature());
        // Extract claims from the payload
        String userId = decodedJWT.getClaim("userId").asString();
        String email = decodedJWT.getClaim("email").asString();

        System.out.println("UserId: " + userId);
        System.out.println("Email: " + email);


        byte[] decodedBytes = Base64.getUrlDecoder().decode(decodedJWT.getPayload());
        String decodPayload = new String(decodedBytes);
        System.out.println(decodPayload);

       DocumentContext jp =JsonPath.parse(decodPayload);
      String iss=jp.read("$.iss");
      System.out.println(iss);
 }

}
