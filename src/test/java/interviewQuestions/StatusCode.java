package interviewQuestions;

public enum StatusCode {
   CODE_200(200,"SUCCESS MSG"),

    CODE_201(201,"SUCCESFULLY CREATED"),

    CODE_400(400,"BAD REQUEST");
    private final int code;
    private final String msg;
    StatusCode(int code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
