package interviewQuestions;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import io.restassured.response.Response;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import wiremockTesting.WireMockUtilityAdvancedScenario;

import java.io.*;

import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class AdvancedWiremockTesting {
    private WireMockServer wireMockServer;

    @BeforeMethod
    public void setup() {
        // Start the WireMock server on port 8080
        wireMockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().port(3030));
        wireMockServer.start();
        configureFor("localhost", 3030);

        // Configure WireMock to stub a POST request
        // wireMockServer.stubFor();
    }


    @Test
    public void testProductOfferingViaFileResponse() throws IOException {
        WireMockUtilityAdvancedScenario.returnProductDetailsViaJsonFileResponse(wireMockServer);
        given().baseUri("http://localhost:3030/")
                .header("Authorization", "Bearer 1234").log().all()
                .when().get("/productOffering")
                .then().log().all();
    }

    @Test
    public void verrifyPaginationConcept() {
        WireMockUtilityAdvancedScenario.queryparamApiWithPagination(wireMockServer, 2, 5);
        given().baseUri("http://localhost:3030/")
                .queryParam("Page", 2)
                .queryParam("Size", 5).log().all()
                .when().get("/api/resource")
                .then().log().all();
    }

    @Test
    public void testInternalServerError() {
        WireMockUtilityAdvancedScenario.internalServerErrorTesting(wireMockServer);
        Response response = given().baseUri("http://localhost:3030/")
                .when().get("/api/server-error")
                .then().log().all().extract().response();
        System.out.println(response.getTime());

    }

    @Test
    public void apiLatencyTesting() {
        WireMockUtilityAdvancedScenario.apiWorkingSlowly(wireMockServer);
        Response response = given().baseUri("http://localhost:3030/")
                .when().get("/api/delayed-response")
                .then().log().all().extract().response();

        System.out.println(response.getTime());
    }

    @Test
    public void multipartFileTesting() {
        WireMockUtilityAdvancedScenario.setupMultipartStub(wireMockServer);
        given().baseUri("http://localhost:3030/")
                .multiPart("file", new File("src/test/resources/ApiFileUpload"))
                .multiPart("description", "Test file upload").log().all()
                .when()
                .post("/upload")
                .then()
                .statusCode(200)
                .body("message", equalTo("File uploaded successfully")).log().all();

    }
    @Test
    public void formParamDirect(){
        WireMockUtilityAdvancedScenario.FormParameter2Test(wireMockServer);
        given().baseUri("http://localhost:3030/")
                .formParam("username","vaibhav")
                .formParam("password","password")
                .cookie("jsessionId","abc123xyc12312z")
                .log().all()
                .when().post("/login").then().log().all().assertThat().statusCode(200);
    }
}
