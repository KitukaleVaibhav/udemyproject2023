package interviewQuestions;

import org.testng.annotations.Test;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteCodeLearning {

    @Test
    public void storeBinaryFile(){
        // Example binary data: array of integers
        int[] data = {123, 456, 789, 101112};

        // Specify the file path
        String filePath = "data.bin";

        // Use try-with-resources to ensure the stream is closed after use
        try (FileOutputStream fos = new FileOutputStream(filePath);
             DataOutputStream dos = new DataOutputStream(fos)) {

            // Write each integer to the binary file
            for (int value : data) {
                dos.writeInt(value); // Writes the integer as 4 bytes
            }

            System.out.println("Data has been written to " + filePath);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testCharToBinary(){
        char character = 'A';
        int asciiValue = (int) character; // Convert char to ASCII integer value
        String binaryString = String.format("%8s", Integer.toBinaryString(asciiValue)).replace(' ', '0'); // Convert to binary with padding
        System.out.println("Character: " + character);
        System.out.println("ASCII Value: " + asciiValue);
        System.out.println("Binary Representation: " + binaryString);
    }
}

