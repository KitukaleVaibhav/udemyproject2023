package interviewQuestions;

public enum SubjectEnum {
    HISTORY(10),
    ENGLISH(20),
    MARATHI(30);

    private int value;

    SubjectEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
