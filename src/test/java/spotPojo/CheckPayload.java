package spotPojo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class CheckPayload {
    @Test
    public void pLoad() throws JsonProcessingException {
        ExternalUrls externalUrls=new ExternalUrls();
        externalUrls.setSpotify("spotif s");
        Followers followers=new Followers();
        followers.setHref("href s");
        followers.setTotal(10);
        Tracks tracks=new Tracks();
        tracks.setHref("href 1");
        tracks.setTotal(2);
        tracks.setNext("df");
        tracks.setLimit(30);
        tracks.setOffset(5);
        tracks.setPrevious(5);
        List<Object> items=new ArrayList<>();
        tracks.setItems(items);
        Owner owner=new Owner();
        owner.setHref("herff ");
        owner.setUri("ursood");
        owner.setType("bdsjd");
        owner.setDisplayName("original sinfk");
        owner.setExternalUrls(externalUrls);
        Playlist playlist=new Playlist();
        playlist.setId("10");
        playlist.setDescription("check sescription");
        playlist.setPublic(false);
        playlist.setName("mosan");
        playlist.setType("original");
        playlist.setCollaborative(false);
        playlist.setHref("hstttd");
        playlist.setSnapshotId("10");
        playlist.setUri("strif");
        playlist.setImages("ddf");
        playlist.setPrimaryColor("Red");
        playlist.setExternalUrls(externalUrls);
        playlist.setOwner(owner);
        playlist.setFollowers(followers);
        playlist.setTracks(tracks);
        ObjectMapper mapper=new ObjectMapper();
        String payload=mapper.writeValueAsString(playlist);
       System.out.println(payload);

    }
}
