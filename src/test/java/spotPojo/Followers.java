package spotPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Followers {
    @JsonProperty("href")
    private Object href; // assuming href can be of any type

    @JsonProperty("total")
    private int total;
}
