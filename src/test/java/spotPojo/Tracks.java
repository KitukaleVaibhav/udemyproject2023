package spotPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
public class Tracks {
    @JsonProperty("href")
    private String href;

    @JsonProperty("items")
    private List<Object> items; // assuming items can be any type

    @JsonProperty("limit")
    private int limit;

    @JsonProperty("next")
    private Object next; // assuming next can be any type

    @JsonProperty("offset")
    private int offset;

    @JsonProperty("previous")
    private Object previous; // assuming previous can be any type

    @JsonProperty("total")
    private int total;
}
