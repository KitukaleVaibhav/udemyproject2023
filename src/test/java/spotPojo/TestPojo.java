package spotPojo;

import SpotifyApiTesting.utils.TestBaseClass;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class TestPojo extends TestBaseClass {
    @Owner("vaibhav")
    @Epic("Playlist")
    @Test
    public void getPlaylistTesting() {
        Playlist pj = given(requestSpecification).basePath("/v1/playlists")
                .when().get("/5gZDorSR85eDGpDvhh68lp").then().extract().body().as(Playlist.class);
        System.out.println(pj.getDescription());
        Assert.assertEquals(pj.getDescription(), "tess", "check decription here");
    }
}
