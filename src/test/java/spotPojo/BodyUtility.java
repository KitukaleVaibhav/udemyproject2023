package spotPojo;

import io.qameta.allure.Step;

import java.util.List;

public class BodyUtility {
    @Step("setFollowers")
    public static Followers setFollowersData(Followers followers,int total,Object href){
        followers.setTotal(total);
        followers.setHref(href);
        return followers;
    }
    @Step("setOwners")
    public static Owner setOwnerData(Owner owner,String type,String dName,String id,String uri,String href){
      //  owner.setExternalUrls(externalUrls);
        owner.setType(type);
        owner.setDisplayName(dName);
        owner.setId(id);
        owner.setUri(uri);
        owner.setHref(href);
        return owner;
    }
    @Step(" set ExternalUrls")
    public static ExternalUrls setExternal(ExternalUrls externalUrls,String spotify){
        externalUrls.setSpotify(spotify);
        return externalUrls;
    }
    @Step("set Tracks")
    public static Tracks setTracks(Tracks tracks, List<Object> items,int offset,Object previous,String href,int limit,Object next,int total){
        tracks.setItems(items);
        tracks.setOffset(offset);
        tracks.setPrevious(previous);
        tracks.setHref(href);
        tracks.setLimit(limit);
        tracks.setNext(next);
        tracks.setTotal(total);
        return tracks;
}
@Step("set Playlist")
public static Playlist setPlaylist(Playlist playlist,boolean publics,String name,boolean collborative,String description,Object images,String uri,String href,String type,Object pcolor,String snapId){
        playlist.setPublic(publics);
        playlist.setName(name);
        playlist.setCollaborative(collborative);
        playlist.setDescription(description);
        playlist.setImages(images);
        playlist.setUri(uri);
        playlist.setHref(href);
        playlist.setType(type);
        playlist.setPrimaryColor(pcolor);
        playlist.setSnapshotId(snapId);
        return playlist;

}
}