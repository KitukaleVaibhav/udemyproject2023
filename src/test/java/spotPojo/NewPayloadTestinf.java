package spotPojo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class NewPayloadTestinf {
    @Test
    public void testP() throws JsonProcessingException {
        ApplicationContext context = new AnnotationConfigApplicationContext(DiTesting.class);
        Playlist pj = context.getBean(Playlist.class);
        BodyUtility.setFollowersData(pj.getFollowers(), 10, "htmmmd");
        BodyUtility.setExternal(pj.getExternalUrls(), "spotify");
        BodyUtility.setOwnerData(pj.getOwner(), "typeq", "name1", "id1", "htlld", "hdytyus");
        List<Object> item = new ArrayList<>();
        item.add(10);
        item.add(50);
        BodyUtility.setTracks(pj.getTracks(), item, 6, "previous", "hrefhdd", 2, "nest", 10);
        BodyUtility.setPlaylist(pj,true,"namet",false,"description 1","image1","type1","red","id1","j","hdjd");
        ObjectMapper mapper=new ObjectMapper();
        String payload=mapper.writeValueAsString(pj);
        System.out.println(payload);
        System.out.println("total:"+pj.getFollowers().getTotal());
    }
}
