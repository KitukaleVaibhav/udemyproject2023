package spotPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter@Setter
@Component
public class Playlist {
    @JsonProperty("collaborative")
    private boolean collaborative;

    @JsonProperty("description")
    private String description;

    @Autowired
    @JsonProperty("external_urls")
    private ExternalUrls externalUrls;

    @Autowired
    @JsonProperty("followers")
    private Followers followers;

    @JsonProperty("href")
    private String href;

    @JsonProperty("id")
    private String id;

    @JsonProperty("images")
    private Object images; // assuming images can be of any type or could be a list of images

    @JsonProperty("name")
    private String name;
    @Autowired
    @JsonProperty("owner")
    private Owner owner;

    @JsonProperty("primary_color")
    private Object primaryColor; // assuming primary color can be any type

    @JsonProperty("public")
    private boolean isPublic;

    @JsonProperty("snapshot_id")
    private String snapshotId;
    @Autowired
    @JsonProperty("tracks")
    private Tracks tracks;

    @JsonProperty("type")
    private String type;

    @JsonProperty("uri")
    private String uri;

}
