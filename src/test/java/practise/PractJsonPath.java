package practise;

import SpotifyApiTesting.utils.ConfigLoader;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.*;

import static io.restassured.RestAssured.given;

public class PractJsonPath {
    @Test
    public void getIdofAllWorkspaces() {

        Response response = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .when().get("/workspaces").then().log().all().extract().response();
        List<String> as = JsonPath.read(response.asString(), "$.workspaces[*].id");
        List<Integer> l1 = JsonPath.read(response.asString(), "$.workspaces[*].id");
        System.out.println(l1.size());
        System.out.println("ids:" + as);
        for (String aq : as) {
            System.out.println(aq);
        }

    }

    @Test
    public void testJwayJsonPath() {

        Response response = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", ConfigLoader.getInstance().getPropertyValue("api-key"))
                .when().get("/workspaces").then().extract().response();
        System.out.println(response.prettyPrint());
        List<Object> as = JsonPath.read(response.asString(), "$.workspaces[1:5]");
        /*for (Object aq : as) {
            System.out.println(aq);
            int id = JsonPath.read(aq, "$.id");
            System.out.println(id);*/

        DocumentContext r1 = JsonPath.parse(response.asString());
        String name = r1.read("$.workspaces[0].name");
        System.out.println("workspace name: " + name);
        List<String> ids = r1.read("$..id");
        for (String id1 : ids) {
            System.out.println("id received" + id1);
        }
    }


    @Test
    public void testTrial() {
        Response response = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a").
                when().get("/workspaces").
                then().extract().response();
        System.out.println(response.statusCode());
        System.out.println(response.body().prettyPrint());
    }

    @Test
    public void checkDuplicate() {
        List<Integer> l1 = new ArrayList<>();
        l1.add(10);
        l1.add(20);
        l1.add(30);
        l1.add(40);
        l1.add(10);
        l1.add(40);
        l1.add(40);
        System.out.println(l1);

        HashMap<Integer, Integer> h1 = new HashMap<>();


        List<Integer> l2 = new ArrayList<>();
        for (Integer v1 : l1) {
            if (h1.containsKey(v1)) {
                h1.put(v1, h1.get(v1) + 1);
                if (h1.get(v1) <= 2) {
                    System.out.println("key: " + v1 + " value: " + h1.get(v1));
                    l2.add(v1);
                }
            } else {
                h1.put(v1, 1);
            }
        }

        System.out.println(h1);
        System.out.println("list of duplicate id'Root1:" + l2);

        Set<Map.Entry<Integer, Integer>> s2 = h1.entrySet();
        for (Map.Entry<Integer, Integer> m2 : s2) {
            if (m2.getValue() > 1) {
                System.out.println("key: " + m2.getKey() + " value:" + m2.getValue());
            }

        }


    }

}
