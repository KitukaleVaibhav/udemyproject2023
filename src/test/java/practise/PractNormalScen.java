package practise;


import static io.restassured.RestAssured.*;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


public class PractNormalScen {

    @Test
    public void test1() {
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when().get("/workspaces").then().assertThat().body("workspaces[0].id", is(equalTo("270ec3d8-fcd8-4f4c-8710-cdc444644f3e")));
    }

    @Test
    public void testWorkspaces() {
        JsonPath jp = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when().get("/workspaces").then().extract().response().jsonPath();
        List<Object> ls = jp.getList("workspaces");
        for (int a = 0; a < ls.size(); a++) {
            //System.out.println(ls.get(a));
            String path = "workspaces[" + a + "]" + ".name";
            //System.out.println("path: "+path);
            String name = jp.getString(path);
            System.out.println("name of workspace " + a + " is:" + name);

        }
    }

    @Test
    public void fetchValueFromObject() {
        JsonPath jp = given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-668f7d025a65d00001b8362e-e5c40b9f13a3c6bf9a7aeda72ef3a8315a")
                .when().get("/workspaces").then().extract().response().jsonPath();
        Object jp1 = jp.getJsonObject("workspaces[2]");

        if (jp1 instanceof Map) {
            Map<String, Object> workspace = (Map<String, Object>) jp1;

            // Fetch individual values using keys
            String id = (String) workspace.get("id");
            String name = (String) workspace.get("name");
            String type = (String) workspace.get("type");

            // Print the values
            System.out.println("Workspace ID: " + id);
            System.out.println("Workspace Name: " + name);
            System.out.println("Workspace Type: " + type);
        } else {
            System.out.println("The response does not contain a workspace object in the expected format.");
        }
    }


}


