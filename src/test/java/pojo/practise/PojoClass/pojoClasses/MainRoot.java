package pojo.practise.PojoClass.pojoClasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties
public class MainRoot {
private int page;

    public MainRoot(int page, int per_page, int total_pages, List<Datum> data, Support support, int total) {
        this.page = page;
        this.per_page = per_page;
        this.total_pages = total_pages;
        this.data = data;
        this.support = support;
        this.total = total;
    }

    public MainRoot(){

}
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }
    private int per_page;
private int total_pages;
private List<Datum> data;
private Support support;
private int total;
}
