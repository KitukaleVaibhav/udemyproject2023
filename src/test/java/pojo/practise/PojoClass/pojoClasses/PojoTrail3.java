package pojo.practise.PojoClass.pojoClasses;

import java.util.List;

public class PojoTrail3 {
    public PojoTrail3(List<PojoTrail2> pojoTrail2List) {
        this.pojoTrail2List = pojoTrail2List;
    }

    public List<PojoTrail2> getPojoTrail2List() {
        return pojoTrail2List;
    }

    public void setPojoTrail2List(List<PojoTrail2> pojoTrail2List) {
        this.pojoTrail2List = pojoTrail2List;
    }

    List<PojoTrail2> pojoTrail2List;
}
