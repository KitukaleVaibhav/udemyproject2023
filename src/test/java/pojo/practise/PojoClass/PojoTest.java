package pojo.practise.PojoClass;

import org.testng.annotations.Test;
import pojo.practise.PojoClass.pojoClasses.Datum;
import pojo.practise.PojoClass.pojoClasses.MainRoot;
import pojo.practise.PojoClass.pojoClasses.PojoTrail2;
import pojo.practise.PojoClass.pojoClasses.PojoTrail3;

import java.util.List;

import static io.restassured.RestAssured.given;

public class PojoTest {
   // @Test
    public void test() {
        MainRoot desrialize = given().when().get("https://reqres.in/api/users?page=2").then().log().all().extract().response().as(MainRoot.class);
        List<Datum> datas = desrialize.getData();
        int datasSize = datas.size();
        for (int i = 0; i < datasSize; i++) {
            String email = datas.get(i).getEmail();
            System.out.println("email present" + email);

        }
    }

    @Test
    public void test2() {
        given().when().get("https://jsonplaceholder.typicode.com/todos").then().log().all();

    }
}
