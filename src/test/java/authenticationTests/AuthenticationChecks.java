package authenticationTests;

import org.testng.annotations.Test;

import java.util.Base64;

import static io.restassured.RestAssured.given;

public class AuthenticationChecks {

    @Test
    public void Base64BasicAuthTest(){
        String usernameColonPassword="MyUsername:MyPassword";
       String Base64Encoded= Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());
        System.out.println("Encoded String value: "+ Base64Encoded);
        byte[] decodedString=Base64.getDecoder().decode(Base64Encoded);
        System.out.println("Decoded String value: "+new String(decodedString));
    }
    @Test
    public void basicUathTest(){
        given().baseUri("https://postman-echo.com").auth().basic("vaibhav","vaibhav@123").log().parameters()
                .when().get("/basic-auth")
                .then().log().all();
    }
    @Test
    public void AuthKey(){
        given().baseUri("https://api.getpostman.com")
                .header("x-api-key", "PMAK-653ce3257ea97000382a881c-6d99539919159a4383c5173f106cc503f3")
                .when().get("/workspaces").then().log().all()
                .assertThat().statusCode(200);
    }
}
