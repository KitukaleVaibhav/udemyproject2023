package SpotifyApiTesting.utils;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;

import static io.restassured.RestAssured.given;

public class TokenManager {
    private static String token;
    private static Instant tokenExpirationTime;

    @Step("token checking and generation")
    public static String getToken() {
        if (token == null || isTokenExpired()) {
            generateNewToken();
            System.out.println(" new token created: " + token);
        }
        System.out.println("prvious token: " + token);
        return token;

    }

    public static boolean isTokenExpired() {
        return tokenExpirationTime == null || Instant.now().isAfter(tokenExpirationTime);
    }

    public static void generateNewToken() {
        Response response = given().baseUri("https://accounts.spotify.com/api")
                .formParam("client_id", ConfigLoader.getInstance().getPropertyValue("spotifyClientId"))
                .formParam("client_secret", ConfigLoader.getInstance().getPropertyValue("spotifyClientSecret"))
                .formParam("grant_type", "refresh_token")
                .formParam("refresh_token", ConfigLoader.getInstance().getPropertyValue("spotifyRefreshToken")).log().all()
                .when().post("/token").then().log().all().assertThat().statusCode(200).extract().response();
        token = "Bearer " + response.path("access_token");
        int expiresIn = response.jsonPath().getInt("expires_in");
        tokenExpirationTime = Instant.now().plusSeconds(expiresIn);

    }
    @Step("set token time")
    public static void setTokenTime(int tokenTime) throws IOException {
        File file=new File("src/test/resources/token.properties");
        FileWriter fw=new FileWriter(file);
        fw.write("tokenTime="+tokenTime);
        fw.flush();
    }

    @Test
    public void test(){
        try {
            setTokenTime(100);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
