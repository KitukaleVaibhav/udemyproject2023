package SpotifyApiTesting.utils;

import org.testng.annotations.Test;

import java.util.Properties;

public class ConfigLoader {
    private final Properties properties;
    private static ConfigLoader configLoader;

    private ConfigLoader(){
        properties=PropertyUtils.getConfigtestdata("src/test/resources/userConifg.properties");
    }
    public static ConfigLoader getInstance(){
        if(configLoader==null){
            configLoader=new ConfigLoader();
        }
        return  configLoader;
    }
    public String getPropertyValue(String name){
        String prop=properties.getProperty(name);
        return prop;
    }

}
