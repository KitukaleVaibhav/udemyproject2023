package SpotifyApiTesting.utils;

import io.qameta.allure.Step;
import org.testng.annotations.Test;

import java.io.*;
import java.time.LocalDateTime;
import java.util.Properties;

public class Propertyutils2 {
    @Step("add value in property file")
    public static void addPropertyToFile(String key, String value) {
        String CONFIG_FILE = "src/test/resources/token.properties";
        Properties properties = new Properties();
        try (OutputStream output = new FileOutputStream(CONFIG_FILE, true)) {
            properties.load(new FileInputStream(CONFIG_FILE));
            properties.setProperty(key, value);
            properties.store(output, null);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    @Step("get value from property file")
    public static String getPropertyFromFile(String key) throws IOException {
        String CONFIG_FILE = "src/test/resources/token.properties";
        Properties properties = new Properties();
        properties.load(new FileInputStream(CONFIG_FILE));
        return properties.getProperty(key);
    }

    @Step("Verify token expiry and returns the status")
    public static boolean getTokenStatus() throws IOException {
        boolean tokenStatus;
       // String token = getPropertyFromFile("token");
        String Time = getPropertyFromFile("expireTime");
        LocalDateTime expireTime = LocalDateTime.parse(Time);
        if (LocalDateTime.now().isAfter(expireTime)) {
            System.out.println("token is invalid");
            return tokenStatus = true;
        } else {
            System.out.println("token is valid and ready to use");
            return tokenStatus = false;
        }

    }

    @Step("clear previous token & expiry time from token properties file")
    public static void clearContentOfTokenFile() {
        String filePath = "src/test/resources/token.properties";

        try {
            // Set the length of the file to zero
            RandomAccessFile raf = new RandomAccessFile(filePath, "rw");
            raf.setLength(0);
            raf.close();
            System.out.println("File content cleared successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




