package SpotifyApiTesting.utils;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;

import static SpotifyApiTesting.api.TokenManager.getToken;
import static io.restassured.RestAssured.given;

public class TestBaseClass {
   public static RequestSpecification requestSpecification;
     private static ResponseSpecification responseSpecification;

    @BeforeClass
    @Step("creating and setup the basic configuration and request specification")
    public static RequestSpecification beforeClass() {
        //System.out.println("token received: "+tm.getToken());
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.spotify.com")
                .addHeader("Authorization", TokenManager2.getToken())
                .setContentType(ContentType.JSON)
                .addFilter(new AllureRestAssured());// we use this method to print request and response body in allur report
        requestSpecBuilder.log(LogDetail.ALL);
        return requestSpecification = requestSpecBuilder.build();// if we use the build the it'Root1 mandatory to use  given().spec(requestSpecification); requestSpecifoication format

    }


   @BeforeSuite
   @Step("delete allure old files")
   public void deleteAllurePrevious(){
       String fileFolder = "allure-results";
       File file = new File(fileFolder);
       File[] files = file.listFiles();

       for (File f : files) {
           System.out.println(f.getName());
           f.delete();
       }
       System.out.println("allure result prevuos report deleted");
   }
    }


