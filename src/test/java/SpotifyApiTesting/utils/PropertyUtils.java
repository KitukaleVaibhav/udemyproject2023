package SpotifyApiTesting.utils;

import org.testng.annotations.Test;

import java.io.*;
import java.util.Properties;

public class PropertyUtils {

    static String filePath = "src/test/resources/userConifg.properties";

    public static Properties getConfigtestdata(String filePath) {
        Properties properties = new Properties();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException exception) {
                throw new RuntimeException("failed to load properties file " + filePath);
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return properties;
    }


    @Test
    public void readProperty() {
        System.out.println(getConfigtestdata("username"));
    }


}


