package SpotifyApiTesting.utils;

import com.github.javafaker.Faker;
import org.joda.time.LocalDate;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDateTime.parse;

public class FakerUtils {
    public static void main(String[] args) {
        String dateTimeString = "2024-08-09T18:36:38.173657900Z";

        // Remove the 'Z' and parse the string
        String trimmedDateTimeString = dateTimeString.replace("Z", "");
        LocalDateTime localDateTime = LocalDateTime.parse(trimmedDateTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        LocalDateTime date2 = localDateTime.plusSeconds(3600);
        System.out.println("LocalDateTime2: " + date2);
        System.out.println("LocalDateTime: " + localDateTime);
    }
}

    /*@Test
    public void generatePlaylistname() {
       LocalDateTime ldm = parse("2024-08-09T18:36:38.173657900",DateTimeFormatter.ISO_LOCAL_TIME);
       System.out.println(ldm);
        Faker faker = new Faker();
        String name=faker.regexify("^[a-zA-Z]{20}$");
        System.out.println(name);
    }*/

