package SpotifyApiTesting.utils;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static io.restassured.RestAssured.given;

public class TokenManager2 {

    @Step("get token")
    public static String getToken() {
        String token;
        boolean status;
        try {
            status = Propertyutils2.getTokenStatus();
            if (status == true) {
              String Expiredtoken =Propertyutils2.getPropertyFromFile("token");
                Propertyutils2.clearContentOfTokenFile();
                Propertyutils2.addPropertyToFile("expireToken",Expiredtoken);
                return token = generateNewToken();
            } else {
                return token = Propertyutils2.getPropertyFromFile("token");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

@Step("generate new access token")
    public static String generateNewToken() {
        Response response = given().baseUri("https://accounts.spotify.com/api")
                .formParam("client_id", ConfigLoader.getInstance().getPropertyValue("spotifyClientId"))
                .formParam("client_secret", ConfigLoader.getInstance().getPropertyValue("spotifyClientSecret"))
                .formParam("grant_type", "refresh_token")
                .formParam("refresh_token", ConfigLoader.getInstance().getPropertyValue("spotifyRefreshToken")).log().all()
                .when().post("/token").then().log().all().assertThat().statusCode(200).extract().response();
        String token = "Bearer " + response.path("access_token");
        int expiresIn = response.jsonPath().getInt("expires_in");
        LocalDateTime ldt = LocalDateTime.now();
        LocalDateTime expireTime = ldt.plusSeconds(expiresIn);
        Propertyutils2.addPropertyToFile("expireTime", String.valueOf(expireTime));
        Propertyutils2.addPropertyToFile("token", token);
        return token;
    }


}
