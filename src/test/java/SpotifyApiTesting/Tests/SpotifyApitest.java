package SpotifyApiTesting.Tests;


import SpotifyApiTesting.POJO.spotifyPojo.Playlist;
import SpotifyApiTesting.utils.TestBaseClass;
import io.qameta.allure.*;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static SpotifyApiTesting.api.RouteUrl.USERS;
import static SpotifyApiTesting.api.TokenManager.getToken;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class SpotifyApitest extends TestBaseClass {


    final String  url2="https://vaibhavkitukale2023.atlassian.net/jira/software/c/projects/PROJ/boards/1?selectedIssue=PROJ-3";
    @Test(description = "get album of the artist")
    @Link(name = "spotify album", url = url2)
    @Issue("AUTH-123")
    @TmsLink("TMS-456")
    @Epic("ALbum")
    public void getAlbum() {
        given(requestSpecification).basePath("/v1/albums")
                .when().get("/4aawyAB9vmqN3uQ7FjRGTy")
                .then().log().all().assertThat().statusCode(200);

    }

     @Test
    public void createPlaylist() {
        Playlist playlist=new Playlist();
        playlist.setName("testing 28 aug");
        playlist.setDescription("this is testing of 28 aug");
        playlist.setPublic(false);
        given(requestSpecification).basePath("/v1/users").body(playlist)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(201);

    }

    @Description("Performing negative testCase to check whether user can create playlist with wrong Data")
    @Owner("vaibhav")
    @Feature("playlist feature")
    @Test
    public void shouldNotABletoCreatePlaylistWithErongData() {
        String payload = "{\n" +
                "    \"name\": \"\",\n" +
                "    \"description\": \"empty playlist created\",\n" +
                "    \"public\": false\n" +
                "}";
        given(beforeClass()).basePath("/v1/users").body(payload)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(400)
                .body("error.status", equalTo(400))
                .body("error.message", equalTo("Missing required field: name"));
    }


    @Owner("vaibhav")
    @Epic("Playlist")
    @Test
    @Step("getting palylist")
    public void getPlaylist() {
        given(requestSpecification).accept(ContentType.XML).basePath("/v1/playlists")
                .when().get("/5gZDorSR85eDGpDvhh68lp").then().log().all();
    }

   // @Test
    @Epic("Playlist")
    public void updatePlaylist(){
        String payload="{\n" +
                "    \"name\": \"2024 23 july updated playlist 2\",\n" +
                "    \"description\": \" second time Updated playlist \",\n" +
                "    \"public\": false\n" +
                "}";
        given(requestSpecification).basePath("/v1/playlists")
                .body(payload)
                .when().put("/5gZDorSR85eDGpDvhh68lp").then().log().all().assertThat()
                .statusCode(200);
    }
}
