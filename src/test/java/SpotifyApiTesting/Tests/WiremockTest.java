package SpotifyApiTesting.Tests;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.client.HttpClient;
import org.testng.annotations.Test;

import java.net.http.HttpRequest;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WiremockTest {
    public static void main(String[] args) {
        configureFor("localhost", 3030);

        stubFor(get(urlPathEqualTo("/api/users"))
                .withQueryParam("id", equalTo("123"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{ \"userId\": 123, \"name\": \"John Doe\" }")));
    }

    @Test
    public static void test() {
        TimeUnit nanoseconds = TimeUnit.NANOSECONDS;
        System.out.println(nanoseconds);
    }

}


