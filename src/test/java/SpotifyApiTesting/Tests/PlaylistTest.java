package SpotifyApiTesting.Tests;

import SpotifyApiTesting.POJO.spotifyPojo.Playlist;
import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class PlaylistTest {
    RequestSpecification requestSpecification;

    @BeforeClass
    public void beforeClass() {
        String ReceivedToken = getaccesToken();
        String finalToken = "Bearer " + ReceivedToken;
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.spotify.com")
                .addHeader("Authorization", finalToken)
                .setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();// if we use the build the it'Root1 mandatory to use  given().spec(requestSpecification); requestSpecifoication format

    }

    public String getaccesToken() {
        Response response = given().baseUri("https://accounts.spotify.com/api")
                .formParam("client_id", ConfigLoader.getInstance().getPropertyValue("spotifyClientId"))
                .formParam("client_secret", ConfigLoader.getInstance().getPropertyValue("spotifyClientSecret"))
                .formParam("grant_type", "refresh_token")
                .formParam("refresh_token", ConfigLoader.getInstance().getPropertyValue("spotifyRefreshToken")).log().all()
                .when().post("/token").then().log().all().assertThat().statusCode(200).extract().response();
        String ReceivedAccessToken = response.path("access_token");
        return ReceivedAccessToken;
    }

    @Test
    public void createPlaylist() {
        Playlist requestPlaylist = new Playlist();
        requestPlaylist.setName("datt song");
        requestPlaylist.setDescription("bhaktigite playlist");
        requestPlaylist.setPublic(false);

        Playlist responsePlaylist = given(requestSpecification).basePath("/v1/users").body(requestPlaylist)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(201)
                .extract().response().as(Playlist.class);
        assertThat(responsePlaylist.getName(), equalTo(requestPlaylist.getName()));
        assertThat(responsePlaylist.getDescription(), equalTo(requestPlaylist.getDescription()));
        assertThat(responsePlaylist.getPublic(), equalTo(requestPlaylist.getPublic()));
    }
}
