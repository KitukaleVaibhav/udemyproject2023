package SpotifyApiTesting.POJO.pojoUsingLombok.lombokAnnotationTesting;

import SpotifyApiTesting.POJO.pojoUsingLombok.Playlist;
import SpotifyApiTesting.Tests.SpotifyApitest;
import SpotifyApiTesting.utils.TestBaseClass;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import static SpotifyApiTesting.api.TokenManager.getToken;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BulderAnWithoutBuilderUse extends TestBaseClass {

    public static RequestSpecification getRequestSpecification() {
        RequestSpecification requestSpecification1 = BulderAnWithoutBuilderUse.beforeClass();
        return requestSpecification1;
    }

@Test
    public void withoutUsingBuilderPattern() {
        Playlist requestPlaylist = new Playlist();
        requestPlaylist.setDescription("badshah song 2024 v2");
        requestPlaylist.setName("badshah 2");
        requestPlaylist.set_public(false);
        Playlist responsePlaylist = given(getRequestSpecification()).basePath("/v1/users").body(requestPlaylist)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(201)
                .extract().response().as(Playlist.class);
        assertThat(responsePlaylist.getName(), equalTo(requestPlaylist.getName()));
        assertThat(responsePlaylist.getDescription(), equalTo(requestPlaylist.getDescription()));
        assertThat(responsePlaylist.get_public(), equalTo(requestPlaylist.get_public()));
    }

}
