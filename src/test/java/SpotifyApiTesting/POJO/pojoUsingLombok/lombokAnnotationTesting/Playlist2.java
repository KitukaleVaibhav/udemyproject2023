
package SpotifyApiTesting.POJO.pojoUsingLombok.lombokAnnotationTesting;

import SpotifyApiTesting.POJO.pojoUsingLombok.ExternalUrls;
import SpotifyApiTesting.POJO.pojoUsingLombok.Followers;
import SpotifyApiTesting.POJO.pojoUsingLombok.Owner;
import SpotifyApiTesting.POJO.pojoUsingLombok.Tracks;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;


@Getter @Setter
@Jacksonized
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Playlist2 {
    @JsonProperty("collaborative")
    private Boolean collaborative;

    @JsonProperty("description")
    private String description;

    @JsonProperty("external_urls")
    private ExternalUrls externalUrls;

    @JsonProperty("followers")
    private Followers followers;

    @JsonProperty("href")
    private String href;

    @JsonProperty("id")
    private String id;

    @JsonProperty("images")
    private List<Object> images;


    @JsonProperty("name")
    private String name;


    @JsonProperty("owner")
    private Owner owner;


    @JsonProperty("primary_color")
    private Object primaryColor;


    @JsonProperty("public")
    private Boolean _public;


    @JsonProperty("snapshot_id")
    private String snapshotId;

    @JsonProperty("tracks")
    private Tracks tracks;
    @JsonProperty("type")
    private String type;
    @JsonProperty("uri")
    private String uri;

    Playlist2(Boolean collaborative, String description, ExternalUrls externalUrls, Followers followers, String href, String id, List<Object> images, String name, Owner owner, Object primaryColor, Boolean _public, String snapshotId, Tracks tracks, String type, String uri) {
        this.collaborative = collaborative;
        this.description = description;
        this.externalUrls = externalUrls;
        this.followers = followers;
        this.href = href;
        this.id = id;
        this.images = images;
        this.name = name;
        this.owner = owner;
        this.primaryColor = primaryColor;
        this._public = _public;
        this.snapshotId = snapshotId;
        this.tracks = tracks;
        this.type = type;
        this.uri = uri;
    }

    public static Playlist2Builder builder() {
        return new Playlist2Builder();
    }

    public static class Playlist2Builder {
        private Boolean collaborative;
        private String description;
        private ExternalUrls externalUrls;
        private Followers followers;
        private String href;
        private String id;
        private List<Object> images;
        private String name;
        private Owner owner;
        private Object primaryColor;
        private Boolean _public;
        private String snapshotId;
        private Tracks tracks;
        private String type;
        private String uri;

        Playlist2Builder() {
        }

        public Playlist2Builder collaborative(Boolean collaborative) {
            this.collaborative = collaborative;
            return this;
        }

        public Playlist2Builder description(String description) {
            this.description = description;
            return this;
        }

        public Playlist2Builder externalUrls(ExternalUrls externalUrls) {
            this.externalUrls = externalUrls;
            return this;
        }

        public Playlist2Builder followers(Followers followers) {
            this.followers = followers;
            return this;
        }

        public Playlist2Builder href(String href) {
            this.href = href;
            return this;
        }

        public Playlist2Builder id(String id) {
            this.id = id;
            return this;
        }

        public Playlist2Builder images(List<Object> images) {
            this.images = images;
            return this;
        }

        public Playlist2Builder name(String name) {
            this.name = name;
            return this;
        }

        public Playlist2Builder owner(Owner owner) {
            this.owner = owner;
            return this;
        }

        public Playlist2Builder primaryColor(Object primaryColor) {
            this.primaryColor = primaryColor;
            return this;
        }

        public Playlist2Builder _public(Boolean _public) {
            this._public = _public;
            return this;
        }

        public Playlist2Builder snapshotId(String snapshotId) {
            this.snapshotId = snapshotId;
            return this;
        }

        public Playlist2Builder tracks(Tracks tracks) {
            this.tracks = tracks;
            return this;
        }

        public Playlist2Builder type(String type) {
            this.type = type;
            return this;
        }

        public Playlist2Builder uri(String uri) {
            this.uri = uri;
            return this;
        }

        public Playlist2 build() {
            return new Playlist2(collaborative, description, externalUrls, followers, href, id, images, name, owner, primaryColor, _public, snapshotId, tracks, type, uri);
        }

        public String toString() {
            return "Playlist2.Playlist2Builder(collaborative=" + this.collaborative + ", description=" + this.description + ", externalUrls=" + this.externalUrls + ", followers=" + this.followers + ", href=" + this.href + ", id=" + this.id + ", images=" + this.images + ", name=" + this.name + ", owner=" + this.owner + ", primaryColor=" + this.primaryColor + ", _public=" + this._public + ", snapshotId=" + this.snapshotId + ", tracks=" + this.tracks + ", type=" + this.type + ", uri=" + this.uri + ")";
        }
    }
}
