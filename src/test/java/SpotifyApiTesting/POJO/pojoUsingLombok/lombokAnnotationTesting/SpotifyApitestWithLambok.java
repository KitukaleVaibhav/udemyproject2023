package SpotifyApiTesting.POJO.pojoUsingLombok.lombokAnnotationTesting;

import SpotifyApiTesting.POJO.pojoUsingLombok.ExternalUrls;
import SpotifyApiTesting.POJO.pojoUsingLombok.Playlist;
import SpotifyApiTesting.POJO.pojoUsingLombok.lombokAnnotationTesting.Playlist2;
import SpotifyApiTesting.utils.TestBaseClass;
import io.qameta.allure.*;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class SpotifyApitestWithLambok extends TestBaseClass {





     @Test
     public void createPlaylistUsingLombokWithoutBuliderPattern() {
         Playlist playlist=new Playlist();
         playlist.setName("lombok 11-08");
         playlist.setDescription("this is testing purposes of lamob");
         playlist.set_public(false);
        given(requestSpecification).basePath("/v1/users").body(playlist)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(201);

    }
    @Test
    public void createPlaylistUsingLombokWithBuliderPattern() {
      Playlist2 playlist =Playlist2.builder().name("neha kakkar").description("new neha kakar song 2024")._public(false).build();
        given(requestSpecification).basePath("/v1/users").body(playlist)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(201);

    }




}
