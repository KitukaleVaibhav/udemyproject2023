package SpotifyApiTesting.api;

import SpotifyApiTesting.utils.ConfigLoader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class TokenManager {

    public static String renewToken;
    private static String access_token;
    static Instant expiry_time;


    public static String getToken() {
        try {
            if (access_token == null || Instant.now().isAfter(expiry_time)) {
                Response response = renewToken();
                access_token = response.path("access_token");
                int expiryDurationInSecond = response.path("expires_in");
                expiry_time = Instant.now().plusSeconds(expiryDurationInSecond);
            } else {
                System.out.println("token is not expired yet");
            }
        } catch (Exception e) {
            throw new RuntimeException("failed to get token");
        }
        return access_token;
    }

    public static Response renewToken() {
        String refToken = "AQBgos-GymOrVuzTFgHDULPKdwCdpI3bIGd6udZH9YeLD625nRkFWKI78m_08UXjA1AtLPR_PNHzU3YztHfgzd9rwZM8Uy5f51JdxdnHCMHm6UQBvcLZ-iwCGqKbdEvqrGg";
        HashMap<String, String> formParams = new HashMap<String, String>();
        formParams.put("client_id", "07791a6c4a9c4df5bd6379c937341334");
        formParams.put("client_secret", "c8b7d78039684761bca6d731170d50e2");
        formParams.put("grant_type", "refresh_token");
        formParams.put("refresh_token", refToken);

        Response response = given().baseUri("https://accounts.spotify.com/api")
                .contentType(ContentType.URLENC)
                .formParams(formParams)
                .when().post("/token").then().log().all().assertThat().statusCode(200).extract().response();
        String ReceivedAccessToken = response.path("access_token");


        if (response.statusCode() != 200) {
            throw new RuntimeException("Abort- Renew token failed");
        }
        return response;
    }

    @Test
    public void getPropertyReceive() {
        System.out.println(ConfigLoader.getInstance().getPropertyValue("username"));
    }

}
