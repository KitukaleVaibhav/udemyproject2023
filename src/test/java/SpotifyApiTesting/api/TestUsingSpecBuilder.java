package SpotifyApiTesting.api;

import org.testng.annotations.Test;

import static SpotifyApiTesting.api.SpecBuilder.getRequestSpecification;
import static SpotifyApiTesting.api.SpecBuilder.getResponseSpecification;
import static io.restassured.RestAssured.given;

public class TestUsingSpecBuilder {
    @Test
    public void getAlbum(){
        given(getRequestSpecification()).basePath("/v1/albums")
                .when().get("/4aawyAB9vmqN3uQ7FjRGTy")
                .then().spec(getResponseSpecification());
    }
}
