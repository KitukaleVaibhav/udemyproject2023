package SpotifyApiTesting.api;

public class RouteUrl {
    public static final String BASE_PATH= "/V1";
    public static final String API="/api";
    public static final String TOKEN="/token";
    public static final String USERS="/users";
    public static final String PLAYLIST="/playlist";
}
