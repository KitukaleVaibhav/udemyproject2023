package SpotifyApiTesting.api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.given;

public class SpecBuilder {
    RequestSpecification requestSpecification;
    ResponseSpecification responseSpecification;

    public static String getaccesToken(){
        String refToken="AQBgos-GymOrVuzTFgHDULPKdwCdpI3bIGd6udZH9YeLD625nRkFWKI78m_08UXjA1AtLPR_PNHzU3YztHfgzd9rwZM8Uy5f51JdxdnHCMHm6UQBvcLZ-iwCGqKbdEvqrGg";
        Response response=given().baseUri("https://accounts.spotify.com/api")
                .formParam("client_id","07791a6c4a9c4df5bd6379c937341334")
                .formParam("client_secret","c8b7d78039684761bca6d731170d50e2")
                .formParam("grant_type","refresh_token")
                .formParam("refresh_token",refToken).log().all()
                .when().post("/token").then().log().all().assertThat().statusCode(200).extract().response();
        String ReceivedAccessToken=response.path("access_token");
        return ReceivedAccessToken;
    }

    public static RequestSpecification getRequestSpecification(){
        String ReceivedToken=getaccesToken();
        String finalToken="Bearer "+ReceivedToken;
       return new RequestSpecBuilder().setBaseUri("https://api.spotify.com")
                .addHeader("Authorization",  finalToken)
                .setContentType(ContentType.JSON)
               .log(LogDetail.ALL).build();
    }

    public static ResponseSpecification getResponseSpecification(){
       // ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
       return new ResponseSpecBuilder().log(LogDetail.ALL).build();
    }
}
