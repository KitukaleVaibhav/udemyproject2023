package SpotifyApiTesting.api.applicationApi;

import SpotifyApiTesting.POJO.spotifyPojo.Playlist;
import io.restassured.response.Response;

import static SpotifyApiTesting.api.SpecBuilder.getRequestSpecification;
import static io.restassured.RestAssured.given;

public class PlaylistApi {

    public Response post(Playlist requestPlaylist) {
        return given(getRequestSpecification()).basePath("/v1/users").body(requestPlaylist)
                .when().post("/31xudvasajt5qzdwtliursc3bm6e/playlists")
                .then().log().all().assertThat().statusCode(201)
                .extract().response();
    }

    public Response get(String playlistId) {
        return given(getRequestSpecification()).basePath("/v1/playlists")
                .when().get("/playlistId").then().extract().response();
    }

    public Response update(String playlistId, Playlist requestPlayList) {

        return given(getRequestSpecification()).basePath("/v1/playlists/")
                .body(requestPlayList)
                .when().put(playlistId).then().extract().response();
    }
}

