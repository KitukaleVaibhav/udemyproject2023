package pojoClasses;

public class WorkspaceRoot {
    private Workspace workspace;

    public Workspace getWorkspace() {
        return workspace;
    }
    public WorkspaceRoot () {

    }

    public WorkspaceRoot(Workspace workspace) {
        this.workspace = workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }
}


