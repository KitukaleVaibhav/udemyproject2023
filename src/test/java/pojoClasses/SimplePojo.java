package pojoClasses;

import com.fasterxml.jackson.annotation.JsonProperty;

/*
POJO stands for Plain Old Java Object. It is a Java object that does not have any special restrictions,
requirements, or dependencies imposed by a framework or library.In simpler terms, a POJO class is a regular Java
 class that represents a data entity. It typically consists of private fields, public getters and setters for the fields,
  and may have additional behaviors in the form of methods, constructors, or utility functions.POJO classes are commonly
  used in Object-Oriented Programming to encapsulate and represent data structures in a flexible and easily maintainable way.
   They can be used in various contexts, such as data transfer objects, model classes, or entity classes, depending on the
   specific application or framework being used.
 */
public class SimplePojo {

    @JsonProperty("key1")
    private String key1;

    @JsonProperty("key2")
    private String key2;

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public SimplePojo(String key1, String key2) {
        this.key1 = key1;
        this.key2 = key2;
    }

    public SimplePojo() {
    }
}
