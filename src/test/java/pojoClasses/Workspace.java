package pojoClasses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

// note all the @Json annotation we can use class level, varaible level and method level also

//@JsonIgnoreProperties(value="id",allowGetters = true)
/*
if you wan't to allow properties only at the time deserialization then use allowSetters = true
if you wan't to allow properties only at the time serialization then use allowGetters = true

 */
public class Workspace {
    @JsonInclude(JsonInclude.Include.NON_DEFAULT) // we use this if we use this then it will exclued all those paramter from pojo which we not passed in payload
    private int i; // created to test exclude null int value of paramter with test. it'Root1 not the part of payload.
    private String name;
    private String type;
    private String description;
    @JsonIgnore // if we use this annotation then it will totally igonore this parameter at the time of serialization and deserialization
    private String visibility;

    public void setI(int i) {
        this.i = i;
    }
    public int getI() {
        return i;
    }





    public String getName() {
        return name;
    }


    @JsonInclude(JsonInclude.Include.NON_NULL) /*this like perform action  whatever when we not passed any parameter in payload then by degalut it will those
those parameter and passed with parameter as payload (it passed only those properties in request whose values is not null
*/
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }




    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public Workspace() {

    }


    public Workspace(String name, String type, String description) {
        this.name = name;
        this.type = type;
        this.description = description;
    }

}
